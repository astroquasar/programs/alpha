:orphan:
   
AlphaDist
=========

.. currentmodule:: alphaDist

.. autoclass:: PlotCurve
   :no-inherited-members:

     None
     
   .. rubric:: Methods Summary

   .. autosummary::

      ~extract_results
      ~readfort26
      ~order_list
      ~plot_curves
      ~fit_parabola
      ~fit_linear

   .. rubric:: Methods Documentation
   
   .. automethod:: extract_results
   .. automethod:: readfort26
   .. automethod:: order_list
   .. automethod:: plot_curves
   .. automethod:: fit_parabola
   .. automethod:: fit_linear
