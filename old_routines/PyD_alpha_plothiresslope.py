#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def plothiresslope(instrument,shape,distlist):

    os.chdir(self.home+'/results/first/')
    
    self.instrument = instrument
    self.shape      = shape
    self.distlist   = distlist
        
    fig = figure(figsize=(12,10))
    plt.subplots_adjust(left=0.08, right=0.95, bottom=0.08, top=0.93, hspace=0.2, wspace=0.2)

    #==============================================================================================================
    
    print '\nHIRES all systems combined with slope distortions\n'
    
    ax = plt.subplot(4,3,1)
    title('HIRES combined $\mathrm{(138}$ $\mathrm{systems)}$\n',fontsize=10)
    x,y,dy = getmag('all','comb')
    ax.errorbar(x,y,yerr=dy,fmt='o',ms=5,markeredgecolor='none',ecolor='purple',alpha=0.8)
    ax.axhline(y=0,ls='dotted',color='black')
    ylabel('Amplitude\n',fontsize=8)

    ax = plt.subplot(4,3,4)
    x,y = getchisq('all','comb')
    scatter(x,y,s=25,edgecolors='none',alpha=0.8)
    ax.axhline(y=0,ls='dotted',color='black')
    ylabel('Chi-square\n',fontsize=8)
    
    ax = plt.subplot(4,3,7)
    x,y = getsigmarand('all','comb')
    scatter(x,y,s=25,edgecolors='none',alpha=0.8)
    ax.axhline(y=0,ls='dotted',color='black')
    ylabel('Sigma-rand factor\n',fontsize=8)
    
    ax = plt.subplot(4,3,10)
    x,y,dy = getglobchisq('all','comb')
    scatter(x,y,s=25,edgecolors='none',alpha=0.8)
    ax.axhline(y=0,ls='dotted',color='black')
    xlabel('\nApplied Distortion Slope',fontsize=8)
    ylabel('Global system fitting chi square\n',fontsize=8)
    
    #==============================================================================================================
    
    print '\nHIRES low redshift systems with slope distortions\n'
    
    ax = plt.subplot(4,3,2)
    title('HIRES low-z sample $\mathrm{(74}$ $\mathrm{systems)}$\n',fontsize=10)
    x,y,dy = getmag('all','lowz')
    ax.errorbar(x,y,yerr=dy,fmt='o',ms=5,markeredgecolor='none',ecolor='purple',alpha=0.8)
    ax.axhline(y=0,ls='dotted',color='black')

    x,y = getchisq('all','lowz')
    if '--reset' not in sys.argv:
        xmin,xmax,ymin,ymax = -0.6,0.6,100,300
    else:
        xmin,xmax,ymin,ymax = min(x),max(x),min(y),max(y)
    ax = plt.subplot(4,3,5,xlim=[xmin,xmax],ylim=[ymin,ymax])
    scatter(x,y,s=25,edgecolors='none',alpha=0.8)
    ax.axhline(y=0,ls='dotted',color='black')
#    fitparabola(x,y,ymin,ymax,xmin,xmax,-0.4,0.5)

    ax = plt.subplot(4,3,8)
    x,y = getsigmarand('all','lowz')
    scatter(x,y,s=25,edgecolors='none',alpha=0.8)
    ax.axhline(y=0,ls='dotted',color='black')
    
    ax = plt.subplot(4,3,11)
    x,y,dy = getglobchisq('all','lowz')
    scatter(x,y,s=25,edgecolors='none',alpha=0.8)
    ax.axhline(y=0,ls='dotted',color='black')
    xlabel('\nApplied Distortion Slope',fontsize=8)
    
    #==============================================================================================================
    
    print '\nHIRES high redshift systems with slope distortions\n'
    
    ax = plt.subplot(4,3,3)
    title('HIRES high-z sample $\mathrm{(64}$ $\mathrm{systems)}$\n',fontsize=10)
    x,y,dy = getmag('all','highz')
    ax.errorbar(x,y,yerr=dy,fmt='o',ms=5,markeredgecolor='none',ecolor='purple',alpha=0.8)
    ax.axhline(y=0,ls='dotted',color='black')
    
    ax = plt.subplot(4,3,6)
    x,y = getchisq('all','highz')
    scatter(x,y,s=25,edgecolors='none',alpha=0.8)
    ax.axhline(y=0,ls='dotted',color='black')
    
    ax = plt.subplot(4,3,9)
    x,y = getsigmarand('all','highz')
    scatter(x,y,s=25,edgecolors='none',alpha=0.8)
    ax.axhline(y=0,ls='dotted',color='black')

    ax = plt.subplot(4,3,12)
    x,y,dy = getglobchisq('all','highz')
    scatter(x,y,s=25,edgecolors='none',alpha=0.8)
    ax.axhline(y=0,ls='dotted',color='black')
    xlabel('\nApplied Distortion Slope',fontsize=8)

    savefig('hires_slope.pdf')
    clf()

#==================================================================================================================
