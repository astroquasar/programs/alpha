import alpha

class model_system(object):

    """
    This operation will create plot of the distortion model for each model.
    The distortion model for each exposure is shown in the upper panel and
    the combined distortion model is shown in the bottom panel.
    """
    
    def showhelp(self):

        print ""
        print "-------------------------------------------------------------------------"
        print ""
        print "required arguments:"
        print ""
        print "   --xmin            Minimum wavelength to display in figure"
        print "   --xmax            Maximum wavelength to displat in figure"
        print "   --ymin            Minimum velocity shift to display in figure"
        print "   --ymax            Maximum velocity shift to displat in figure"
        print ""
        print "optional arguments:"
        print ""
        print "   --slope           Distortion slope to apply in the model [0]"
        print ""
        print "example:"
        print ""
        print "   alpha model_system --selection J043037-485523/1.35560/UVES_squader \ "
        print "                      --xmin 2800 --xmax 11000 --ymin -400 --ymax 400 \ "
        print "                      --slope 0.2"
        print ""
        print "-------------------------------------------------------------------------"
        print ""
        quit()
        
    def __init__(self):

        # Display help message or check if required arguments are given
        if '--help' in sys.argv or '-h' in sys.argv:
            self.showhelp()
        path = setup.home+'/results/models/systems/'
        os.system('mkdir -p '+path)
        os.chdir(path)
        slope = setup.slope
        xmin,xmax = setup.xmin,setup.xmax
        ymin,ymax = setup.ymin,setup.ymax
        xlist = [3000,4000,5000,6000,7000,8000,9000,10000]
        ylist = [-300,-200,-100,0,100,200,300]
        publist = [str(i) for i in setup.dipole['system']]
        for i in range(len(setup.publist)):
            system = setup.publist['system'][i]
            quasar = system.split('/')[0]
            sample = system.split('/')[2]
            if sample=='UVES_squader' and (setup.selection=='published' or system==setup.selection):
                print system
                ions  = np.loadtxt(setup.fitdir+system+'/model00/model/turbulent.13',delimiter='\n',dtype=str)
                trans = []
                flag  = 0
                for line in ions:
                    if '*' in line and flag==1:
                        break
                    elif '*' in line:
                        flag = 1
                    else:
                        trans.append((float(line.split()[2])+float(line.split()[3]))/2)
                fig = figure(figsize=(7,4))
                plt.subplots_adjust(left=0.12, right=0.95, bottom=0.12, top=0.95, hspace=0, wspace=0.1)
                ax = plt.subplot(211,xlim=[xmin,xmax],ylim=[ymin,ymax])
                # Distortion models and fitting regions
                exp1 = np.empty((0,4))
                exp2 = np.empty((0,4))
                pos  = np.where(setup.uvesexp['name']==quasar)[0][0]
                for l in range (pos,len(setup.uvesexp)):
                    if setup.uvesexp['name'][l]!=quasar:
                        break
                    for k in range(len(setup.uvesset)):
                        cond1 = setup.uvesset['cent'][k]==setup.uvesexp['cent'][l]
                        cond2 = setup.uvesset['arm'][k]==setup.uvesexp['arm'][l]
                        cond3 = setup.uvesset['mode'][k]==setup.uvesexp['mode'][l]
                        if cond1==cond2==cond3==True:
                            wbeg = 10*float(setup.uvesset['TS_min'][k])
                            cent = 10*float(setup.uvesset['cent'][k])
                            wend = 10*float(setup.uvesset['TS_max'][k])
                            time = float(setup.uvesexp['exptime'][l])
                            if cent not in [float(j) for j in exp1[:,2]]:
                                color = 'blue' if setup.uvesexp['arm'][l]=='BLUE' else 'red'
                                exp1 = np.vstack((exp1,[color,wbeg,cent,wend]))
                            exp2 = np.vstack((exp2,[time,wbeg,cent,wend]))
                            break
                for k in range (len(exp1)):
                    color   = exp1[k,0]
                    wastart = float(exp1[k,1])
                    wacent  = float(exp1[k,2])
                    waend   = float(exp1[k,3])
                    x       = np.arange(wastart,waend,1)
                    y       = slope*(x-wacent)
                    ax.plot(x,y,color=color,lw=1.5,zorder=3)
                    ax.axvline(x=wastart,color='black',zorder=2,lw=1,ls='dotted')
                    ax.axvspan(wastart,waend,facecolor='yellow',alpha=0.1,zorder=1)
                    ax.axvline(x=waend,color='black',zorder=2,lw=1,ls='dotted')
                    ax.scatter(wacent,0,color='black',marker='d',edgecolors='none',s=25,zorder=4)
                ax.axhline(y=0,ls='dotted',color='black')
                ax.xaxis.set_major_locator(plt.FixedLocator(xlist))
                ax.yaxis.set_major_locator(plt.FixedLocator(ylist))
                plt.setp(ax.get_xticklabels(), visible=False)
                ylabel('Velocity shift (m/s)',size=10)
                for i in range(len(trans)):
                    ax.axvline(x=trans[i],ymin=0.85,ymax=0.9,color='red',lw=1)
                # Correction function
                ax = plt.subplot(212,xlim=[xmin,xmax],ylim=[ymin,ymax])
                for k in range (len(exp1)):
                    wastart = float(exp1[k,1])
                    wacent  = float(exp1[k,2])
                    waend   = float(exp1[k,3])
                    ax.axvline(x=wastart,color='black',zorder=2,lw=1,ls='dotted')
                    ax.axvspan(wastart,waend,facecolor='yellow',alpha=0.1)
                    ax.axvline(x=waend,color='black',zorder=2,lw=1,ls='dotted')
                x,y = np.arange(3000,10500,1),[]
                for wa in x:
                    vdist,texp = [],[]
                    for i in range (len(exp2)):
                        if exp2[i,1] < wa < exp2[i,3]:
                            vdist.append(slope*(wa-exp2[i,2]))
                            texp.append(np.sqrt(exp2[i,0]))
                    if vdist!=[]:
                        y.append(sum(np.array(texp)*np.array(vdist))/sum(np.array(texp)))
                    else:
                        y.append(None)
                ax.plot(x,y,color='black',lw=1.5,zorder=1)
                ax.axhline(y=0,ls='dotted',color='black')
                ax.xaxis.set_major_locator(plt.FixedLocator(xlist))
                ax.yaxis.set_major_locator(plt.FixedLocator(ylist))
                for i in range(len(trans)):
                    ax.axvline(x=trans[i],ymin=0.85,ymax=0.9,color='red',lw=1)
                xlabel('Wavelength ($\mathrm{\AA}$)',size=12)
                ylabel('Velocity shift (m/s)',size=12)
                savefig(system.replace('/','_').replace('.','z')+'.pdf')
                plt.close(fig)
