import alpha,numpy,sys,os,seaborn
import matplotlib.pyplot as plt

class alphaz:
    """
    Plot da/a vs. redshift for various fitting results.

    **Arguments**

    --error  Plot histogram of uncertainties
    --bin    Number of data points per bin

    Examples
    --------
    >>> alpha alphaz --bin 12
    """
    def __init__(self):

        os.chdir(args.home+'/results/alpha/')
        if '--error' in sys.argv:
            plt.rc('font', size=20, family='sans-serif')
            plt.rc('axes', labelsize=14, linewidth=0.2)
            plt.rc('legend', fontsize=14, handlelength=10)
            plt.rc('xtick', labelsize=14)
            plt.rc('ytick', labelsize=14)
            plt.rc('lines', lw=0.2, mew=0.2)
            plt.rc('grid', linewidth=1)
            fig    = plt.figure(figsize=(12,6))
            ax     = plt.subplot(111,xlim=[0,80])
            data1  = numpy.loadtxt(args.Crep+'uves/reproduce/results.dat',usecols=[4]) * 10
            data2a = numpy.loadtxt(args.Crep+'hires/reproduce/high_contrast/results.dat',usecols=[4]) * 10
            data2b = numpy.loadtxt(args.Crep+'hires/reproduce/all_contrast/results.dat',usecols=[4])[len(data2a):] * 10
            data2  = numpy.hstack((data2a,data2b))
            bins   = numpy.histogram(numpy.hstack((data1,data2)), bins=60)[1]
            plt.subplots_adjust(left=0.06, right=0.97, bottom=0.15, top=0.92, hspace=0.05, wspace=0.05)
    	    ax.hist(data1,bins,histtype='stepfilled',fc='none',lw=3,ec='b',alpha=0.6)
    	    ax.hist(data2,bins,histtype='stepfilled',fc='none',lw=3,ec='r',alpha=0.6)
            plt.set_xlabel(r'$\sigma_{\Delta\alpha/\alpha}$ (ppm)',fontsize=20 )
            plt.savefig('error.pdf')
            plt.clf()
            quit()

        plt.rc('font', size=10, family='sans-serif')
        plt.rc('axes', labelsize=20, linewidth=0.2)
        plt.rc('legend', fontsize=20, handlelength=10)
        plt.rc('xtick', labelsize=20)
        plt.rc('ytick', labelsize=20)
        plt.rc('lines', lw=1, mew=1)
        plt.rc('grid', linewidth=1)

        target         = 'alphafit_errmod'
        uves_original  = numpy.loadtxt(args.Crep+'uves/reproduce/%s.dat'%target,usecols=[0,3,4],dtype=float)
        uves_original  = numpy.array(sorted(uves_original,key=lambda col: col[0]))
        comb_original  = numpy.loadtxt(args.Crep+'comb/reproduce/%s.dat'%target,usecols=[0,3,4],dtype=float)
        comb_original  = numpy.array(sorted(comb_original,key=lambda col: col[0]))
        hires1         = numpy.loadtxt(args.Crep+'hires/reproduce/high_contrast/%s.dat'%target,usecols=[0,3,4],dtype=float)
        hires2         = numpy.loadtxt(args.Crep+'hires/reproduce/all_contrast/%s.dat'%target,usecols=[0,3,4],dtype=float)
        hires_original = numpy.vstack((hires1,hires2[len(hires1):]))
        hires_original = numpy.array(sorted(hires_original,key=lambda col: col[0]))

        self.title = 'UVES sample | Real data | Updated results from VPFIT10'
        sample = 'uves/vd17a/normal/v10_chisq1E-06_all/0.000/%s.dat'%target
        self.plotfig(uves_original,sample,'uves/da_vs_z_01.pdf')
        self.title = 'UVES sample | Real data | Model at -0.123 m/s/A | DW distortion | MoM results'
        sample = 'uves/vd17b/normal/v10_chisq1E-06_all/m0.123/%s.dat'%target
        self.plotfig(uves_original,sample,'uves/da_vs_z_02.pdf')
        #self.title = 'UVES sample | Real data | Model at -0.123 m/s/A | DW distortion | Best chisq model'
        #sample = '/uves/distortion/bestchisq/v10_chisq1E-06_all/m0.123/%s.dat'%target
        #self.plotfig(uves_original,sample,'uves/da_vs_z_03.pdf')
        #self.title = 'UVES sample | Real data | Model at -0.123 m/s/A | DW distortion | Turbulent only'
        #sample = '/uves/distortion/turbulent/v10_chisq1E-06_all/m0.123/%s.dat'%target
        #self.plotfig(uves_original,sample,'uves/da_vs_z_04.pdf')
        #self.title = 'UVES sample | Real data | Model at -0.123 m/s/A | DW distortion | Thermal only'
        #sample = '/uves/distortion/thermal/v10_chisq1E-06_all/m0.123/%s.dat'%target
        #self.plotfig(uves_original,sample,'uves/da_vs_z_05.pdf')
        #self.title = 'UVES sample | Realistic simulation | Model at +0.123 m/s/A | DW distortion'
        #sample = 'uves/simulation/complex_0.000/v10_chisq1E-06/p0.123/%s.dat'%target
        #self.plotsim(uves_original,sample,'uves/da_vs_z_06.pdf')
        #self.title = 'UVES sample | Real data | Model at -0.117 m/s/A | DW distortion'
        #sample = 'uves/distortion/normal/v10_chisq1E-06_all/m0.117/%s.dat'%target
        #self.plotfig(uves_original,sample,'uves/da_vs_z_07.pdf')
        #self.title = 'UVES sample | Simplistic simulation | Model at +0.117 m/s/A | WM distortion'
        #sample = 'uves/simulation/simple_0.000/v10_chisq1E-06_jw/p0.117/%s.dat'%target
        #self.plotsim(uves_original,sample,'uves/da_vs_z_08.pdf')
        self.title = 'UVES sample | Real data | Updated results from VPFIT10'
        sample = 'uves/vd17c/normal/v10_chisq1E-06_all/0.000/%s.dat'%target
        self.plotfig(uves_original,sample,'uves/da_vs_z_09.pdf')
        
        self.title = 'HIRES sample | Real data | Updated results from VPFIT10 | MoM results'
        sample = 'hires/vd17a/normal/v10_chisq1E-06/0.000/%s.dat'%target
        self.plotfig(hires_original,sample,'hires/da_vs_z_01.pdf')
        self.title = 'HIRES sample | Real data | Updated results from VPFIT10 | Original model'
        sample = 'hires/vd17a/normal/v10_chisq1E-06/0.000/%s.dat'%target
        self.plotfig(hires_original,sample,'hires/da_vs_z_02.pdf')
        self.binning = 10
        self.title = 'HIRES sample | Real data | Updated results from VPFIT10'
        sample = 'hires/vd17c/normal/v10_chisq1E-06/0.000/%s.dat'%target
        self.plotfig(hires_original,sample,'hires/da_vs_z_03.pdf')
        
        self.binning = 20
        self.title = 'UVES+HIRES sample | Real data | Updated results from VPFIT10 | MoM results'
        sample = 'comb/vd17a/hires_0.000-uves_0.000/%s.dat'%target
        self.plotfig(comb_original,sample,'comb/da_vs_z_01.pdf')        
        self.title = 'UVES+HIRES sample | Real data | Updated results from VPFIT10 | MoM results'
        sample = 'comb/vd17c/hires_0.000-uves_0.000/%s.dat'%target
        self.plotfig(comb_original,sample,'comb/da_vs_z_02.pdf')
        self.binning = 20
        self.title = 'UVES+HIRES sample | Real data | Updated results from VPFIT10 | MoM results'
        sample = 'comb/vd17c/hires_0.000-uves_m0.123/%s.dat'%target
        self.plotfig(comb_original,sample,'comb/da_vs_z_03.pdf')
        
        #''' Plot da/a vs. distance to alpha dipole '''
        #
        #bindata = self.alphabin(data_distance)
        #fig = plt.figure(figsize=(10,5))
        #plt.subplots_adjust(left=0.1, right=0.95, bottom=0.15, top=0.95, hspace=0.05, wspace=0)
        #ax = plt.subplot(111,xlim=[0,180],ylim=[-2,2])
        #ax.errorbar(bindata[:,1],bindata[:,2],yerr=bindata[:,3],fmt='o',ms=10,lw=3,c='black',capsize=10)
        #ax.errorbar(bindata[:,1],bindata[:,4],yerr=bindata[:,5],fmt='o',ms=10,lw=3,alpha=0.8,c='red',capsize=10)
        #ax.xaxis.set_minor_locator(plt.MultipleLocator(1))
        #ax.yaxis.set_minor_locator(plt.MultipleLocator(0.1))
        #ax.axhline(y=0,color='black')
        #plt.set_xlabel(r'$\Theta$, angle from dipole ('+str(xdip)+','+str(ydip)+')',fontsize=15)
        #plt.set_ylabel(r'MoM $\Delta\alpha/\alpha$ $(10^{-5})$',fontsize=15)
        #plt.savefig('da_vs_angle-'+self.test+'-'+self.distortion+'-'+str(binning)+'.pdf')
        #plt.clf()    
        #
        #''' Plot whole sky map '''
        #
        #fig = plt.figure(figsize=(11.69,8.27))
        #plt.subplots_adjust(left=0.05, right=0.95, bottom=0.1, top=0.95, hspace=0.05, wspace=0)
        #
        #m = Basemap(projection='moll',lat_0=0,lon_0=180)
        #m.drawparallels(numpy.arange(-90.,90,10.))
        #m.drawmeridians(numpy.arange(0,360,20))
        #sbig,smax = 0,5
        #for i in range (len(self.restable)):
        #    if self.restable['MoM_alpha'][i] not in ['0.0000E+00','-']:
        #        sbig = sbig+1 if float(self.restable['MoM_alpha'][i])>smax else sbig
        #        size = 50 if abs(float(self.restable['MoM_da'][i]))>5 else float(self.restable['MoM_da'][i])/5*50
        #        xpt,ypt = m(float(self.restable['ra'][i]),float(self.restable['dec'][i]))
        #        mark  = 's' if self.restable['sample'][i] in keck else 'o'
        #        scatter(xpt,ypt,c=float(self.restable['MoM_alpha'][i]),edgecolor='none',\
        #                s=size,marker=mark,cmap=mpl.cm.cool,vmin=-smax,vmax=smax)
        #    
        #ax1  = fig.add_axes([0.3,0.1,0.4,0.05])
        #cmap = mpl.cm.cool
        #norm = mpl.colors.Normalize(vmin=-smax,vmax=smax)
        #cb1  = mpl.colorbar.ColorbarBase(ax1,cmap=cmap,norm=norm,orientation='horizontal')
        #cb1.set_label(r'$\Delta\alpha/\alpha$ $(10^{-5})$')
        #
        #print sbig,'measurements larger than absolute 5e-5'
        #plt.savefig('map.pdf')
        #plt.clf()

    def plotsim(self,Udata1,path2,name,clip=False):

        Udata2 = numpy.loadtxt(args.Crep+path2,usecols=[0,3,4],dtype=object)
        Bdata1 = self.alphabin(Udata1)
        Bdata2 = self.alphabin(Udata2,visible=True)
        
        x1,y1,x2,y2 = Udata2[:,0],Udata2[:,1],Bdata2[:,0],Bdata2[:,1]
        x3,y3,yerr3 = Bdata1[:,0],Bdata1[:,1],Bdata1[:,2]
        x4,y4,yerr4 = Bdata2[:,0],Bdata1[:,1]-Bdata2[:,1],Bdata1[:,2]
            
        fig = plt.figure(figsize=(12,8))
        plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95, hspace=0.05, wspace=0)
        ax1 = plt.subplot(211,xlim=[0.25,4],ylim=[-2.5,2.5])
        ax1.errorbar(x1,y1,fmt='o',ms=5,lw=3,alpha=0.5,capsize=10,capthick=2,mec='none',color='red',zorder=3)
        ax1.errorbar(x2,y2,fmt='o',ms=20,lw=3,alpha=0.8,capsize=10,capthick=2,mec='none',color='red',zorder=2)
        ax1.errorbar(x3,y3,yerr=yerr3,fmt='o',ms=20,lw=3,alpha=0.7,capsize=10,capthick=2,mec='none',color='black',zorder=1)
        ax1.xaxis.set_minor_locator(plt.MultipleLocator(0.1))
        ax1.yaxis.set_minor_locator(plt.MultipleLocator(0.1))
        ax1.axhline(y=0,color='black')
        ax1.set_title(self.title)
        plt.setp(ax1.get_xticklabels(), visible=False)
        plt.set_ylabel(r'$\Delta\alpha/\alpha$ $(10^{-5})$',fontsize=20)
        ax2 = plt.subplot(212,xlim=[0.25,4],ylim=[-2.5,2.5])
        ax2.errorbar(x4,y4,yerr=yerr4,fmt='o',ms=20,lw=3,alpha=0.8,capsize=10,capthick=2,mec='none',color='black')
        ax2.xaxis.set_minor_locator(plt.MultipleLocator(0.1))
        ax2.yaxis.set_minor_locator(plt.MultipleLocator(0.1))
        ax2.axhline(y=0,color='black')
        plt.set_ylabel(r'$\Delta\alpha/\alpha$ $(10^{-5})$',fontsize=20)
        plt.set_xlabel('Redshift $z$',fontsize=20)
        plt.savefig(name)
        plt.clf()

    def plotfig(self,Udata1,path2,name):

        Udata2  = numpy.loadtxt(args.Crep+path2,usecols=[0,3,4],dtype=object)
        Bdata1  = self.alphabin(Udata1,visible=True)
        Bdata2  = self.alphabin(Udata2,visible=True)

        x3,y3,yerr3 = Bdata1[:,0],Bdata1[:,1],Bdata1[:,2]
        x4,y4,yerr4 = Bdata2[:,0],Bdata2[:,1],Bdata2[:,2]
            
        fig = plt.figure(figsize=(12,6))
        plt.subplots_adjust(left=0.1, right=0.95, bottom=0.11, top=0.95, hspace=0.05, wspace=0)
        ax1 = plt.subplot(111,xlim=[0.25,4],ylim=[-2.5,2.5])
        ax1.errorbar(x3,y3,yerr=yerr3,fmt='o',ms=20,lw=3,alpha=0.7,capsize=10,capthick=2,mec='none',color='black')
        ax1.errorbar(x4,y4,yerr=yerr4,fmt='o',ms=20,lw=3,alpha=0.8,capsize=10,capthick=2,mec='none',color='red')
        ax1.xaxis.set_minor_locator(plt.MultipleLocator(0.1))
        ax1.yaxis.set_minor_locator(plt.MultipleLocator(0.1))
        ax1.axhline(y=0,color='black')
        #ax1.set_title(self.title)
        ax1.set_ylabel(r'$\Delta\alpha/\alpha$ $(10^{-5})$',fontsize=20)
        ax1.set_xlabel('Redshift $z$',fontsize=20)
        plt.savefig(name)
        plt.clf()
        
    def alphabin(self,data,visible=False):

        k = 1
        data = numpy.array(sorted(data,key=lambda col: col[0]))
        bindata = numpy.empty((0,3),dtype=float)
        for i in range(0,len(data),args.binning):
            ilim  = i+args.binning if i+args.binning<=len(data) else len(data)
            if visible:
                print '\nbin',k,'\n'
            for j in range(i,ilim):
                if visible:
                    print 'z={0:<9}  |  {1:>10} +/- {2:>10}  |  {3:<10}'.format('%.5f'%float(data[j,0]),'%.6f'%float(data[j,1]),'%.6f'%float(data[j,2]),str(data[j,-1]))
            zabs  = numpy.average([float(data[j,0]) for j in range(i,ilim)])
            alpha = sum([float(data[j,1])/float(data[j,2])**2 for j in range(i,ilim)]) / sum([1/float(data[j,2])**2 for j in range(i,ilim)])
            error = 1 / numpy.sqrt(sum(1/float(data[j,2])**2 for j in range(i,ilim)))
            if visible:
                print '------------ | ---------------------------'
                print 'z={0:<9}  |  {1:>10} +/- {2:>10}'.format('%.5f'%zabs,'%.6f'%alpha,'%.6f'%error)
            bindata   = numpy.vstack((bindata,[zabs,alpha,error]))
            k += 1
        return bindata
