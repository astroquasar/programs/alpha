#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def sim_simple():

    self.regtotal = self.regboth = self.regblue = self.regred = 0    

    idxs = []
    for i in range(len(self.distsim)):
        cond0  = self.selection in [self.distsim['system'][i],'published']
        cond1  = self.distsim['system'][i] not in self.outliers
        cond2  = self.distsim['instrument'][i].lower()==self.instrument
        cond3a = self.distsim['whitmore'][i]=='yes' and '--whitmore' in sys.argv
        cond3b = self.distsim['whitmore'][i]=='no'  and '--whitmore' not in sys.argv
        if cond0 and cond1 and cond2 and (cond3a or cond3b):
            idxs.append(i)
            
    for i in idxs:
        
        self.selection = self.distsim['system'][i]
        self.qso       = self.distsim['system'][i].split('/')[0]
        self.zabs      = self.distsim['system'][i].split('/')[1]
        self.sample    = self.distsim['system'][i].split('/')[2]

        print '|- System:',self.selection
        print '|  |- Model-Test:',self.model,'-',self.test
        print '|  |  |- Distortion slope:',self.distortion

        # If default synthetic spectrum not created, do it

        simpath = self.fitdir+self.selection+'/'+self.model+'/sims/'+self.simtest
        if os.path.exists(simpath+'/model/')==True:
            
            print '|  |  |  |- Spectra already available.'
            
        if os.path.exists(simpath+'/model/')==False:
            
            print '|  |  |  |- Reading original fort.13...'
            
            os.system('mkdir -p '+simpath+'/model/original')
            os.system('mkdir -p '+simpath+'/model/data')
            os.system('cp '+self.fitdir+self.selection+'/'+self.model+'/model/header.dat '+simpath+'/model/')
            
            # Read original fort.13

            wbeg,wend = None,None
            header = np.loadtxt(self.fitdir+self.selection+'/'+self.model+'/model/header.dat',delimiter='\n',dtype=str)
            fort13 = np.loadtxt(self.fitdir+self.selection+'/'+self.model+'/model/turbulent.13',delimiter='\n',dtype=str)
            regs   = np.empty((0,5),dtype=object)
            ions   = []
            i,flag = 0,0
            while i < len(fort13):
                if '*' in fort13[i]:
                    flag += 1
                    i += 1
                    k = 0
                if flag==1:
                    if 'external' not in header[k]:
                        val  = header[k].split()[0]
                        ion  = val.split('_')[0]
                        wave = int(float(val.split('_')[1]))
                        wmid = float(self.atominfo(val)[1])
                        wbeg = (wmid*(1+float(self.zabs))) * (2*self.c-50) / (2*self.c+50)
                        wend = (wmid*(1+float(self.zabs))) * (2*self.c+50) / (2*self.c-50)
                        regs = np.vstack((regs,[self.zabs,ion,wmid,wbeg,wend]))
                        if ion not in ions:
                            ions.append(ion)
                    k += 1
                elif flag==2:
                    break
                i += 1
        
            # Generate artificial spectrum
            
            os.chdir(simpath+'/model/')
            
            print '|  |  |  |- Generating artificial spectrum...'
            
            fwhm  = 5.0
            noise = 2000.
            col   = np.array([['MgI',11.28],['MgII',13.08],['AlII',11.98],['AlIII',11.38],
                              ['SiII',13.06],['CrII',11.18],['FeII',13.00],['MnII',11.03],
                              ['NiII',11.75],['TiII',11.46],['ZnII',11.18],['SiIV',13.06]],dtype=object)
            
            os.system('cp '+self.atomdir+' atom.dat')
            opfile = open('vp_setup.dat','w')
            for row in self.vpfsetup:
                opfile.write(row+'\n')
            opfile.close()
            os.environ['ATOMDIR']='./atom.dat'
            os.environ['VPFSETUP']='./vp_setup.dat'

            for i in range(len(regs)):#len(regs)):

                wbeg = float(regs[i,3])-10
                wend = float(regs[i,3])+10
                disp = 1.3
                wave = [wbeg]
                while wave[-1]<wend:
                    wave.append(wave[-1]*(2*self.c+disp)/(2*self.c-disp))
                np.savetxt('wave%02i.dat'%(i+1), np.transpose([wave, np.ones_like(wave), np.ones_like(wave), ]))
                
                rdgen = open('./commands.dat','w')
                rdgen.write('rd wave%02i.dat \n'%(i+1))
                rdgen.write('gp\n')
                for ion in ions:
                    if ion in col[:,0]:
                        k = np.where(col[:,0]==ion)[0][0]
                        rdgen.write(ion+' '+str(col[k,1])+' 2.5 '+self.zabs+'\n')
                    else:
                        print ion,'not found in Whitmore list...'
                        quit()
                rdgen.write('\n')           # Terminate selection
                rdgen.write(str(fwhm)+'\n')        # Instrumental profile FWHM
                rdgen.write('noise\n')      # Add noise
                rdgen.write('\n')           # Don't select random number seed
                rdgen.write(str(noise)+'\n')      # SNR in pix-1
                rdgen.write('wt spec%02i.dat (all)\n'%(i+1))
                rdgen.write('lo\n')
                rdgen.close()
        
                os.system('rdgen < commands.dat > termout')
                os.system('rm termout commands.dat')
                
                data  = np.loadtxt('spec%02i.dat'%(i+1))
                shift = float(self.getshift(float(regs[i,-2]),float(regs[i,-1]),self.slope))
                spec  = open('spec%02i.dat'%(i+1),'w')
                for i in range(len(data)):
                    wa = data[i,0]*(2*self.c+shift)/(2*self.c-shift) if '--distspec' in sys.argv else data[i,0]
                    fl = data[i,1]
                    er = data[i,2]
                    spec.write('{0:>20}{1:>20}{2:>20}\n'.format('%.15f'%wa,'%.15f'%fl,'%.15f'%er))
                spec.close()
                                    
            os.system('mkdir -p data && rm wave* && mv spec* data/')
            
            # Create thermal.13 of simulated system
    
            print '|  |  |  |- Create thermal.13 of simulated system...'
    
            fort13 = open('./thermal.13','w')
            fort13.write('*\n')
            for i in range(len(regs)):
                wbeg  = '%.2f'%float(regs[i,3])
                wend  = '%.2f'%float(regs[i,4])
                trans = regs[i,1]+'_'+str(int(float(regs[i,2])))
                fort13.write('{0:<15}{1:>2}{2:>10}{3:>10}  vfwhm={4:<5} ! {5:<10}\n'.format('data/spec%02i.dat'%(i+1),'1',wbeg,wend,str(fwhm),trans))
            fort13.write('*\n')
            for ion in ions:
                k = np.where(col[:,0]==ion)[0][0]
                N = '%.7f'%col[k,1]
                b = '2.5000000'
                z = self.zabs+'a' if ion==ions[0] else self.zabs+'A'
                a = '0.00000q'    if ion==ions[0] else '0.00000Q'
                fort13.write('  {0:<8}  {1:<10}  {2:<10}  {3:<10}  {4:<10}  0.00   0.00E+00  0\n'.format(ion,N,z,b,a))
            if self.slope!=0 and '--distspec' not in sys.argv:
                for i in range(len(regs)):
                    shift = self.getshift(float(regs[i,-2]),float(regs[i,-1]),self.slope)
                    fort13.write('  >>  1.00000FF  0.0000000FF '+shift+'FF  0.000FF  0.00  0.00E+00  '+str(i+1)+'\n')
            fort13.close()

        # Fitting the system

        self.distpath = simpath+'/'+self.test+'/'+self.distortion+'/'
        os.system('mkdir -p '+self.distpath)
        os.chdir(self.distpath)

        if self.prepare('thermal')=='proceed':
            print '|  |  |  |- Processing Thermal fitting...'
            self.fit('thermal')

        if '--compress' in sys.argv:
            
            destination = self.here.replace('list','execute')+'/'
            os.chdir(self.fitdir)
            target   = self.selection+'/'+self.model+'/sims/'+self.simtest+'/'+self.test+'/'+self.distortion
            filename = target.replace('/','--')
            os.system('tar -zcvf '+filename+'.tar.gz '+target+'/')
            os.system('rm -rf '+target+'/')
            os.system('mkdir -p '+destination)
            os.system('mv '+filename+'.tar.gz '+destination)

#==================================================================================================================
