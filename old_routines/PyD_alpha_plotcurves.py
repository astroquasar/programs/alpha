#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def plotcurves(fit=False,loop=False):
    
    rc('font', size=2, family='sans-serif')
    rc('axes', labelsize=10, linewidth=0.2)
    rc('legend', fontsize=10, handlelength=10)
    rc('xtick', labelsize=7)
    rc('ytick', labelsize=7)
    rc('lines', lw=0.2, mew=0.2)
    rc('grid', linewidth=0.2)
    
    fig = figure(figsize=(8,5))
    plt.subplots_adjust(left=0.05, right=0.97, bottom=0.05, top=0.95, hspace=0.2, wspace=0.2)
    model = ['Thermal','Turbulent','MoM']
    for j in range(len(model)):

        if loop==True:
            self.out.write('{0:>10}\t'.format('%.2f'%np.mean(self.fitres[:,2+4*j])))
        
        x     = self.fitres[:,0]
        y     = self.fitres[:,2+4*j]
        label = model[j]+' $\chi^2$'
        ax    = plt.subplot(2,3,1+j,xlim=[self.distmin,self.distmax])
        ax.errorbar(x,y,fmt='o',ms=4,markeredgecolor='none',ecolor='grey',alpha=0.7,color='black')
        ymin  = 0 if len(y)==0 else min(y)-1 if min(y)==max(y) else min(y)
        ymax  = 1 if len(y)==0 else max(y)+1 if min(y)==max(y) else max(y)
        if self.selection not in self.degensys and ('--fit' in sys.argv or fit==True):
            self.fitparabola2(x,y,model=model[j])
        ylim(ymin,ymax)
        y_formatter = matplotlib.ticker.ScalarFormatter(useOffset=False)
        ax.yaxis.set_major_formatter(y_formatter)
        t1 = ax.text((self.distmin+self.distmax)/2,ymax-0.1*(ymax-ymin),self.selection,color='grey',ha='center',fontsize=6)
        t1.set_bbox(dict(color='white', alpha=0.7, edgecolor=None))
        ax.set_title(label,fontsize=10)

        if loop==True:
            slope = 'badwhit' if self.selection in self.badwhit and '--whitmore' in sys.argv else 'degenerate' if self.selection in self.degensys else '%.4f'%self.slope
            error = 'badwhit' if self.selection in self.badwhit and '--whitmore' in sys.argv else 'degenerate' if self.selection in self.degensys else '%.4f'%self.slope_error
            self.out.write('{0:>10}\t{1:>10}\t'.format(slope,error))            
            
        x     = self.fitres[:,0]
        y     = self.fitres[:,3+4*j]
        yerr  = self.fitres[:,4+4*j]
        ymin  = 0 if len(y)==0 else min(y)-1 if min(y)==max(y) else min(y-yerr)
        ymax  = 1 if len(y)==0 else max(y)+1 if min(y)==max(y) else max(y+yerr)
        label = model[j]+' $da/a$ $(10^{-5})$'
        ax    = plt.subplot(2,3,4+j,xlim=[self.distmin,self.distmax],ylim=[ymin,ymax])
        ax.errorbar(x,y,yerr=yerr,fmt='o',ms=4,markeredgecolor='none',ecolor='grey',alpha=0.7,color='black')
        if self.selection not in self.degensys and ('--fit2' in sys.argv or fit==True):
            self.fitlinear(x,y,yerr,model=model[j])
        y_formatter = matplotlib.ticker.ScalarFormatter(useOffset=False)
        ax.yaxis.set_major_formatter(y_formatter)
        t1 = ax.text((self.distmin+self.distmax)/2,ymax-0.1*(ymax-ymin),self.selection,color='grey',ha='center',fontsize=6)
        t1.set_bbox(dict(color='white', alpha=0.7, edgecolor=None))
        ax.set_title(label,fontsize=10)
        ax.axhline(y=0,ls='dotted',lw=0.1,color='black')
        
        if loop==True:
            alpha = 'badwhit' if self.selection in self.badwhit and '--whitmore' in sys.argv else 'degenerate' if self.selection in self.degensys else '%.4f'%self.alpha
            error = 'badwhit' if self.selection in self.badwhit and '--whitmore' in sys.argv else 'degenerate' if self.selection in self.degensys else '%.4f'%self.alpha_error
            self.out.write('{0:>10}\t{1:>10}\t'.format(alpha,error))            
            
    if loop==True:
        self.out.write('\n')
        
    os.system('mkdir -p '+self.name)
    savefig(self.name+'/'+self.selection.replace('/','_').replace('.','z')+'.pdf')
    plt.close(fig)

#==================================================================================================================
            
def plotweighted(mode):

    rc('font', size=5, family='sans-serif')
    rc('axes', labelsize=10, linewidth=0.2)
    rc('legend', fontsize=10, handlelength=10)
    rc('xtick', labelsize=10)
    rc('ytick', labelsize=10)
    rc('lines', lw=0.2, mew=0.2)
    rc('grid', linewidth=0.2)
    
    fig = figure(figsize=(5,8))
    plt.subplots_adjust(left=0.15, right=0.95, bottom=0.07, top=0.95, hspace=0.3, wspace=0)
    model = ['Thermal','Turbulent','MoM']
    for i in range(len(model)):
        
        if mode=='model':
            
            x    = self.results[:,0]
            y    = self.results[:,1+i]
            
            xmin = -0.1
            xmax = 0
            imin = abs(x-xmin).argmin()
            imax = abs(x-xmax).argmin()
            ymin = min(y[imin:imax])
            ymax = max(y[imin:imax])
            
            imid = abs(y-min(y)).argmin()
            isig = abs(y-(min(y)+1)).argmin()
            xmid = x[imid]
            xsig = abs(x[isig]-x[imid])
            xm1sig = xmid-xsig
            xp1sig = xmid+xsig
            
            print 'Slope: {0:>8}+/-{1:<8}'.format('%.4f'%xmid,'%.4f'%xsig),
            
            ax = plt.subplot(3,1,i+1)
            ax.plot(x,y,color='black',alpha=0.7,lw=2)
            axvline(x=xm1sig,ls='dotted',color='red',lw=1)
            axvline(x=xmid,ls='dashed',color='red',lw=1)
            axvline(x=xp1sig,ls='dotted',color='red',lw=1)
            t1 = text(0,ymax-0.17*(ymax-ymin),'$1\sigma$ : %.4f+/-%.4f'%(xmid,xsig),color='red',fontsize=6,ha='center')
            t1.set_bbox(dict(color='white', alpha=0.7, edgecolor=None))
            
        if mode=='global':
            
            x = self.results[:,0]
            y = self.results[:,2*(i+1)]

            xmin = self.distmin
            xmax = self.distmax
            ymin = min(y)
            ymax = max(y)
            
#            imin = abs(x+0.03).argmin()
#            imax = abs(x+0.14).argmin()
            
            A = np.vander(x[8:-1],3)
            (coeffs, residuals, rank, sing_vals) = np.linalg.lstsq(A,y[8:-1])
            f = np.poly1d(coeffs)
            xfit = np.arange(-50,50,0.0001)
            imid = abs(f(xfit)-min(f(xfit))).argmin()
            isig = abs(f(xfit)-(min(f(xfit))+1)).argmin()
            xmid = xfit[imid]
            xsig = abs(xfit[isig]-xfit[imid])
            xm1sig = xmid-xsig
            xp1sig = xmid+xsig
            
            print 'Slope: {0:>8}+/-{1:<8}'.format('%.4f'%xmid,'%.4f'%xsig),
            
            ax = plt.subplot(3,1,i+1)
            ax.scatter(x,y,color='black',alpha=0.7,lw=2)
            ax.plot(xfit,f(xfit),c='red',lw=1)
            axvline(x=xm1sig,ls='dotted',color='red',lw=1)
            axvline(x=xmid,ls='dashed',color='red',lw=1)
            axvline(x=xp1sig,ls='dotted',color='red',lw=1)
            t1 = text((xmin+xmax)/2,ymax-0.17*(ymax-ymin),'$1\sigma$ : %.4f+/-%.4f'%(xmid,xsig),color='red',fontsize=10,ha='center')
            t1.set_bbox(dict(color='white', alpha=0.7, edgecolor=None))

        if i==0 and '--whitmore' in sys.argv:
            plt.title('Simplistic model',fontsize=10)
        elif i==0:
            plt.title('Our model',fontsize=10)
        ax.set_xlim(xmin,xmax)
        ax.set_ylim(ymin,ymax)
        ax.axhline(y=0,ls='dotted',color='black')
        ax.axvline(x=0,color='black')
#        ax.set_title(model[i],color='red',fontsize=10)
        ax.set_xlabel('Applied distortion slope (in m/s/$\AA$)',fontsize=10)
        ax.set_ylabel(model[i]+' global absolute $\chi^2$',fontsize=10)
        ax.yaxis.set_major_formatter(matplotlib.ticker.ScalarFormatter(useOffset=False))
        
    savefig(self.name+'.pdf')
    clf()
    
#==================================================================================================================
