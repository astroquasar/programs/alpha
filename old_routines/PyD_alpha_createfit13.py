#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def createfit13(model,mode=None):

    flag26 = flag18 = 0
    
    final = open(model+'_fit.13','w')

    final.write('   *\n')
    line26 = np.loadtxt('fort.26',dtype='str',delimiter='\n')
    for i in range(len(line26)):
        if 'Stats:' in line26[i]:
            flag26 = 1
        if line26[i][0:2]!='%%':
            break
        else:
            final.write(line26[i].replace('%% ','')+'\n')
    final.write('  *\n')
            
    line18 = np.loadtxt('fort.18',dtype='str',delimiter='\n')
    for i in range(len(line18)-1,0,-1):
        if 'statistics for whole fit:' in line18[i]:
            flag18 = 1
        if 'chi-squared' in line18[i]:
            chisq   = '%.4f'%float(line18[i].split('(')[1].split(',')[0])
            chisqnu = '%.3f'%float(line18[i].split('(')[0].split(':')[1])
            ndf     = '%.0f'%float(line18[i].split(')')[0].split(',')[1])
            if mode!='sim':
                print '|  |  |  |  |- chisq='+chisq+', ndf='+ndf+', chisq_nu='+chisqnu
            a = i + 2
            break
    for i in range(a,len(line18)):
        if len(line18[i])==1:
            break
        final.write(line18[i]+'\n')

    final.close()

    if '--raijin' in sys.argv and mode!='sim':
        os.system('cp '+model+'_fit.13 '+self.distpath+'/../')

    if flag26==1 and flag18==1 and mode!='sim':
        os.system('mv fort.26 '+model+'.26')
        os.system('mv fort.18 '+model+'.18')
    
#==================================================================================================================
