#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def plotmixed():

    os.chdir(self.home+'/results/models/')
    
    slope = 0.2#0.117
    xmin,xmax = 3000,10000
    ymin,ymax = -400,400
    xlist = [4000,5000,6000,7000,8000,9000]
    ylist = [-300,-200,-100,0,100,200,300]
    
    for i in range(len(self.dipole)):

        system     = self.dipole['system'][i]
        quasar     = system.split('/')[0]
        sample     = system.split('/')[2]
        instrument = self.dipole['instrument'][i].lower()
        cond1      = self.selection=='published' or system==self.selection
        cond2      = self.sample==None or sample==self.sample
        cond3      = self.instrument in sample.lower()

        if cond1 and cond2 and cond3:

            print system
            
            path      = self.fitdir+system+'/model00/model/'
            model     = 'turbulent.13' if os.path.exists(path+'turbulent.13')==True else 'thermal.13'
            ions      = np.loadtxt(path+model,delimiter='\n',dtype=str)
            trans     = []
            flag      = 0
            for line in ions:
                if '*' in line and flag==1:
                    break
                elif '*' in line:
                    flag = 1
                else:
                    trans.append((float(line.split()[2])+float(line.split()[3]))/2)
                    
            fig = figure(figsize=(8,5))
            plt.subplots_adjust(left=0.12, right=0.9, bottom=0.12, top=0.9, hspace=0, wspace=0)
            fig.suptitle(system,fontsize=12)
            
            ''' Plot Whitmore distortion function'''
            
            ax = plt.subplot(223,xlim=[xmin,xmax],ylim=[ymin,ymax])
            ax.axhline(y=0,ls='dotted',color='black')
            
            if 'UVES_squader' in system:
                
                x = np.arange(xmin,3259.1,1)
                y = [-slope*3900+slope*3259.1 for i in range(len(x))]
                ax.plot(x,y,'black',ls='dashed',lw=2)

                x0 = 3900
                x = np.arange(3259.1,4518.8,1)
                y = slope*(x-x0)
                ax.plot(x,y,'blue',lw=2)
                
                x  = np.arange(4518.8,4726.9,1)
                y1 = -slope*3900 + slope*4518.8
                y2 = -slope*5800 + slope*4726.9
                a  = (y2-y1)/(4726.9-4518.8)
                b  = 4518.8 - y1/a
                y  = a*(x-b)
                ax.plot(x,y,'black',ls='dashed',lw=2)
                
                x0 = 5800
                x = np.arange(4726.9,6834.9,1)
                y = slope*(x-x0)
                ax.plot(x,y,'red',lw=2)
                
                x = np.arange(6834.9,xmax,1)
                y = [-slope*5800+slope*6834.9 for i in range(len(x))]
                ax.plot(x,y,'black',ls='dashed',lw=2)
                
            if 'HIRES_sargent' in system:
                
                x = np.arange(xmin,xmax,1)
                y = slope*(x-5500.)
                ax.plot(x,y,'blue',lw=2)
                
            ax.xaxis.set_major_locator(plt.FixedLocator(xlist))
            ax.yaxis.set_major_locator(plt.FixedLocator(ylist))
            ylabel('Velocity shift (m/s)',fontsize=10)
            xlabel('Wavelength ($\AA$)',fontsize=10)
            
            for i in range(len(trans)):
                ax.axvline(x=trans[i],ymin=0.85,ymax=0.9,color='red',lw=1)
                
            ''' Distortion models and fitting regions '''    
            
            ax = plt.subplot(222,xlim=[xmin,xmax],ylim=[ymin,ymax])
            exp1 = np.empty((0,4))
            exp2 = np.empty((0,4))
            
            if 'UVES_squader' in system:
                
                pos  = np.where(self.uvesexp['name']==quasar)[0][0]
                for l in range (pos,len(self.uvesexp)):
                    if self.uvesexp['name'][l]!=quasar:
                        break
                    for k in range(len(self.uvesset)):
                        cond1 = self.uvesset['cent'][k]==self.uvesexp['cent'][l]
                        cond2 = self.uvesset['arm'][k]==self.uvesexp['arm'][l]
                        cond3 = self.uvesset['mode'][k]==self.uvesexp['mode'][l]
                        if cond1==cond2==cond3==True:
                            wbeg = 10*float(self.uvesset['TS_min'][k])
                            cent = 10*float(self.uvesset['cent'][k])
                            wend = 10*float(self.uvesset['TS_max'][k])
                            time = float(self.uvesexp['exptime'][l])
                            if cent not in [float(j) for j in exp1[:,2]]:
                                color = 'blue' if self.uvesexp['arm'][l]=='BLUE' else 'red'
                                exp1 = np.vstack((exp1,[color,wbeg,cent,wend]))
                            exp2 = np.vstack((exp2,[time,wbeg,cent,wend]))
                            break
                        
            if 'HIRES_sargent' in system:
                
                pos  = np.where(self.hiresexp['name']==quasar)[0][0]
                for l in range (pos,len(self.hiresexp)):
                    if self.hiresexp['name'][l]!=quasar:
                        break
                    wbeg = float(self.hiresexp['blue'][l])
                    cent = float(self.hiresexp['cent'][l])
                    wend = float(self.hiresexp['red'][l])
                    time = float(self.hiresexp['exptime'][l])
                    if cent not in [float(j) for j in exp1[:,2]]:
                        color = 'blue'
                        exp1 = np.vstack((exp1,[color,wbeg,cent,wend]))
                        exp2 = np.vstack((exp2,[time,wbeg,cent,wend]))
                        
            for k in range (len(exp1)):
                color   = exp1[k,0]
                wastart = float(exp1[k,1])
                wacent  = float(exp1[k,2])
                waend   = float(exp1[k,3])
                x       = np.arange(wastart,waend,1)
                y       = slope*(x-wacent)
                ax.plot(x,y,color=color,lw=1.5,zorder=1)
                ax.axvline(x=wastart,color='black',zorder=2,lw=1,ls='dotted')
                ax.axvspan(wastart,waend,facecolor='yellow',alpha=0.1)
                ax.axvline(x=waend,color='black',zorder=2,lw=1,ls='dotted')
                
            ax.axhline(y=0,ls='dotted',color='black')
            ax.yaxis.set_major_locator(plt.FixedLocator(ylist))
            plt.setp(ax.get_xticklabels(), visible=False)
            ax.text(2200,-2000,'Velocity shift (m/s)',size=12,rotation='vertical',ha='center',va='center')
            ax.yaxis.tick_right()
            
            for i in range(len(trans)):
                ax.axvline(x=trans[i],ymin=0.85,ymax=0.9,color='red',lw=1)
                
            ''' Correction function '''
            
            ax = plt.subplot(224,xlim=[xmin,xmax],ylim=[ymin,ymax])
            
            for k in range (len(exp1)):
                wastart = float(exp1[k,1])
                wacent  = float(exp1[k,2])
                waend   = float(exp1[k,3])
                ax.axvline(x=wastart,color='black',zorder=2,lw=1,ls='dotted')
                ax.axvspan(wastart,waend,facecolor='yellow',alpha=0.1)
                ax.axvline(x=waend,color='black',zorder=2,lw=1,ls='dotted')
         
            x,y = np.arange(3000,10500,1),[]
            for wa in x:
                vdist,texp = [],[]
                for i in range (len(exp2)):
                    if exp2[i,1] < wa < exp2[i,3]:
                        vdist.append(slope*(wa-exp2[i,2]))
                        texp.append(np.sqrt(exp2[i,0]))
                if vdist!=[]:
                    y.append(sum(np.array(texp)*np.array(vdist))/sum(np.array(texp)))
                else:
                    y.append(None)
            ax.plot(x,y,color='black',lw=1.5,zorder=1)
            ax.axhline(y=0,ls='dotted',color='black')
            ax.yaxis.set_major_locator(plt.FixedLocator(ylist))
            ax.xaxis.set_major_locator(plt.FixedLocator(xlist))
            
            for i in range(len(trans)):
                ax.axvline(x=trans[i],ymin=0.85,ymax=0.9,color='red',lw=1)
         
            xlabel('Wavelength ($\AA$)',size=10)
            ax.yaxis.tick_right()

            savefig('mixed_'+instrument+'/'+system.replace('/','_').replace('.','z')+'.pdf')
            clf()
        
#==================================================================================================================

