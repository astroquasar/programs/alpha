#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def fortlist():

    def isfloat(value):
        try:
          float(value)
          return True
        except ValueError:
          return False

    n = 0
    filename = 'fortlist_'+self.selection.replace('/','--')+'.dat'
    os.system('find '+self.fitdir+' -mindepth 3 -maxdepth 3 | sort > list_'+filename)
    allsys = np.loadtxt('list_'+filename,dtype='str')
    outfile = open(filename,'w')
    for line in allsys:
        
        model   = self.model
        quasar  = line.split('/')[-3]
        redabs  = line.split('/')[-2]
        sample  = line.split('/')[-1]
        system  = quasar+'/'+redabs+'/'+sample

        cond1   = (self.sample==None and sample in self.keck+self.vlt) or self.sample==sample
        cond2a  = self.selection=='published' and system in [str(i) for i in self.publist['system']] and system not in self.outliers
        cond2b  = self.selection in ['all',system]
        cond3a  = self.instrument=='uves' and sample in self.vlt
        cond3b  = self.instrument=='hires' and sample in self.keck
        cond4   = ('--clip' not in sys.argv) or ('--clip' in sys.argv and system not in self.clippeduves)

        if cond1 and (cond2a or cond2b) and (cond3a or cond3b) and cond4:

            if '--distortion' in sys.argv and self.selection=='published' and self.model=='original':
                whitmore = 'yes' if '--whitmore' in sys.argv else 'no'
                i = np.where(np.logical_and(self.distres['system']==system,self.distres['whitmore']==whitmore))[0][0]
                model = 'model%02i'%float(self.distres['model individual'][i])
            if '--simulation' in sys.argv:
                outfile.write(line+'/'+model+'/sims/'+self.simtest+'/'+self.test+'/'+self.distortion+'\n')
            else:
                outfile.write(line+'/'+model+'/runs/'+self.test+'/'+self.distortion+'\n')
                
            n = n + 1

    outfile.close()
    os.system('rm list_'+filename)
    if n==0:
        print '|- ABORT: No absorption systems found, please make sure your arguments are correct.\n'
        quit()

    return filename
        
#==================================================================================================================
