#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def getdipolechisq():

    selection = self.selection+'/threshold/%.3f'%self.threshold if '--threshold' in sys.argv else self.selection

#    print '\n\tAlpha dipole Chi square\n'
    
    chisqval = np.empty((0,3))
    
    for i in self.distlist:
        strslope = '0.000' if round(i,3)==0 \
                   else str('%.3f'%i).replace('-','m') if '-' in str(i) \
                   else 'p'+str('%.3f'%i)
        pathdir  = self.Crep+self.instrument+'/'+selection+'/'+self.test+'/'\
                   +strslope+'/'+self.zsample+'/termout.dat'

        if os.path.exists(pathdir)==True and round(i,2) not in self.discard:
            output  = np.loadtxt(pathdir,dtype='str',delimiter='\n')
            chilist = []
            for k in range (len(output)):
                if 'chisq = ' in output[k]:
                    chisqred = float(output[k].split()[2])
                    chisqabs = float(output[k].split()[5])
                    chisqval = np.vstack((chisqval,[i,chisqabs,chisqred]))
#                    print '\t\t','{:>10}'.format('%.3f'%i),'{:>10}'.format('%.2f'%chisqabs),'{:>10}'.format('%.9f'%chisqred)
                    break

    x,yabs,y = [],[],[]

    if len(chisqval)>0:

        minchisq = min(chisqval[:,1])
        dist = [round(i,3) for i in chisqval[:,0]]
        yabs = chisqval[:,1]
        yred = chisqval[:,2]

        A = np.vander(dist,3)
        (coeffs, residuals, rank, sing_vals) = np.linalg.lstsq(A,yabs)
        f = np.poly1d(coeffs)
        x = np.arange(-0.5,0.5,0.0001)
        imid = abs(f(x)-min(f(x))).argmin()
        imid = abs(dist-x[imid]).argmin()
        minchisq = yred[imid]
#        self.norm = 10**int(math.log10(np.exp(-minchisq/2)*np.exp(-minchisq/2.)))

        y = [np.exp(-minchisq/2)*np.exp(-chisq/2.) for chisq in yred]
#        y = yred
    
    return dist,yabs,y
    
#==================================================================================================================
