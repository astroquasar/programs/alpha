#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def fitgaussian(x,y,xmin,xmax,ymin,ymax,xmin_fit,xmax_fit,flag=None,flag2=None):

    ''' Fit the Gaussian and plot the model '''

    mean,sigma = 0,1
    def gaus(x,a,x0,sigma):
        return a*np.exp(-(x-x0)**2/(2*sigma**2))
    popt,pcov = curve_fit(gaus,x,y,p0=[1,mean,sigma])
    x = np.arange(xmin,xmax,0.001)
    plot(x,gaus(x,*popt),color='red',label='fit',lw=3,alpha=0.9)

    if flag=='plotonesig':

        imid = abs(gaus(x,*popt)-max(gaus(x,*popt))).argmin()
        xmid = x[imid]
        ymid = gaus(xmid,*popt)

#        text(0.1,ymin+0.45*(ymax-ymin),'Optimised value at y = '+'%.3f'%ymid,fontsize=6,color='red')
#        axhline(y=ymid,ls='dashed',color='red',lw=1)
        axvline(x=xmid,ls='dashed',color='red',lw=2)

        if self.xm1sig!=None:
            sig1 = xmid-(self.xmid-self.xm1sig)
            axvline(x=sig1,ls='dotted',color='black',lw=1)
        if self.xp1sig!=None:
            sig1 = xmid+(self.xp1sig-self.xmid)
            axvline(x=sig1,ls='dotted',color='black',lw=1)
        print 'Best fitting model: %.3f +/- %.3f'%(xmid,(sig1-xmid))
        if self.xm1sig!=None or self.xp1sig!=None:
            text(sig1,ymin+0.1*(ymax-ymin),' %.3f'%xmid+' $\pm$ '+'%.3f'%(sig1-xmid),color='red',fontsize=15)
#
#        if self.xm2sig!=None:
#            sig2 = xmid-(self.xmid-self.xm2sig)
#            axvline(x=sig2,ls='dotted',color='black')
#        if self.xp2sig!=None:
#            sig2 = xmid+(self.xp2sig-self.xmid)
#            axvline(x=sig2,ls='dotted',color='black')
#        if self.xm2sig!=None or self.xp2sig!=None:
#            text(-0.5,ymin+0.6*(ymax-ymin),' $2\sigma$: '+'%.3f'%xmid+' $\pm$ '+'%.3f'%(sig2-xmid),color='red',fontsize=6)
#
#        if self.xm3sig!=None:
#            sig3 = xmid-(self.xmid-self.xm3sig)
#            axvline(x=sig3,ls='dotted',color='black')
#        if self.xp3sig!=None:
#            sig3 = xmid+(self.xp3sig-self.xmid)
#            axvline(x=sig3,ls='dotted',color='black')
#        if self.xm3sig!=None or self.xp3sig!=None:
#            text(-0.5,ymin+0.5*(ymax-ymin),' $3\sigma$: '+'%.3f'%xmid+' $\pm$ '+'%.3f'%(sig3-xmid),color='red',fontsize=6)

#==================================================================================================================
