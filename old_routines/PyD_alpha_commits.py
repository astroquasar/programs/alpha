#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def commits():

    print ''
    print '----------------------------------------------------------------------------------'
    print '      List of commits made'
    print '----------------------------------------------------------------------------------'
    print ''
    print '  16/10/11 - Start commenting every line of code'
    print ''
    print '----------------------------------------------------------------------------------'
    print '      Descriptions'
    print '----------------------------------------------------------------------------------'
    print ''
    print '  expind (opt)'
    print ''
    print '      This option allows for the slope to be randomly calculated for each exposure'
    print '      using a given mean slope value and standard deviation. This is used in the'
    print '      sim_complex function when looping over all the fitting regions and looking'
    print '      for the overlapping exposures. The self.shifts array initialized in the'
    print '      sim_complex function will be used to store the slope value for each exposure'
    print '      in order to avoid recalculation of the slopes among the fitted regions.'
    print ''
    print '----------------------------------------------------------------------------------'
    print '      Examples of actions to run'
    print '----------------------------------------------------------------------------------'
    print ''
    print '  Run complex simulations with random slope for each exposure:'
    print ''
    print '      PyD_alpha run --selection J000344-232354/0.45210/UVES_squader --model 1'
    print '                    --simulation complex --expind --stdev 0.05'
    print '                    --distmid -0.123 --distmin -1.123 --distmax 0.877 --distsep 0.1'
    print ''
    print '  Plots curves for each simulations using random slope for each exposure:'
    print ''
    print '      PyD_alpha plots --curve --instrument uves --simulation complex --expind --stdev 0.05'
    print ''
    print '----------------------------------------------------------------------------------'
    print ''

#==================================================================================================================
