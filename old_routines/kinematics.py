import alpha

class kinematics(object):

    """
    Do plot of kinematic structure for every system
    """ 

    def showhelp(self):
        
        print ""
        print "-------------------------------------------------------------------------"
        print ""
        print "required arguments (one of the following):"
        print ""
        print "   --all         Do plot for all the transitions"
        print "   --ion         Do subplot for each ion"
        print ""
        print "optional arguments:"
        print ""
        print "   --bin         Number of value per bin (default: 12)"
        print "   --chunks      Remake all the chunks through VPFIT"
        print "   --instrument  High-resolution spectrograph used"
        print "                   [uves]  VLT spectrograph"
        print "                   [hires] Keck spectrograph"
        print "   --ntrans      Number of transitions"
        print "   --reset       Re-loop over all systems"
        print ""
        print "-------------------------------------------------------------------------"
        print ""
        quit()
        
    def __init__(self):
        
        # Display help message or check if required arguments are given
        if '--help' in sys.argv or '-h' in sys.argv:
            self.showhelp()
        if '--reset' in sys.argv:
            self.extract_data()
        self.create_dataset()
        if '--all' in sys.argv:
            self.equiwidth_vs_dispersion()
            self.equiwidth_vs_tied()
            #self.tiedcomp_vs_redshift()
            #self.equiwidth_vs_redshift()
        if '--ion' in sys.argv:
            self.plot_ion()

    def extract_data(self):

        discard = []
        os.chdir(setup.home+'/results/kinematics/')
        tied = open('list_tied.dat','w')
        tied.write('system  zabs  tied\n')
        metals = open('list_metals.dat','w')
        metals.write('system  trans     spread    ewidth    snr\n')
        for system in setup.publist['system']:
            print 'Scanning',system
            setup.quasar   = system.split('/')[0]
            setup.zabs     = float(system.split('/')[1])
            setup.sample   = system.split('/')[2]
            setup.model    = 2.1 if setup.sample=='UVES_squader' else 7.1
            setup.distpath = setup.fitdir+'/'+system+'/model-%.1f/model/'%setup.model
            os.chdir(setup.distpath)
            header = np.loadtxt(setup.distpath+'header.dat',dtype=str,delimiter='\n')
            fort13 = np.loadtxt(setup.distpath+'thermal.13',dtype=str,delimiter='\n')
            # fix repository if from UVES_squader sample
            if 'UVES_squader' in system:
                os.system('rm data')
                os.system('ln -s ../../data')
                os.system('cp ../runs/v10_chisq1E-06_all/0.000/thermal/atom.dat .')
                os.system('cp ../runs/v10_chisq1E-06_all/0.000/thermal/vp_setup.dat .')
                if os.path.exists('thermal.13.bak')==False:
                    flag = 0
                    os.system('mv thermal.13 thermal.13.bak')
                    ofile = open('thermal.13','w')
                    for line in fort13:
                        flag = flag+1 if '*' in line else flag
                        if flag==1 and '*' not in line:
                            temp = line.replace(line.split()[0],'data/'+line.split()[0])
                            ofile.write(temp+'\n')
                        else:
                            ofile.write(line+'\n')
                    ofile.close()
            # create data chunk for each region
            if '--chunks' in sys.argv:
                if os.path.exists('data'):
                    os.system('rm data')
                os.system('ln -s ../../data')
                os.system('mkdir -p chunks/')
                opfile = open('fitcommands','w')
                opfile.write('d\n')             # Run display command + enter
                opfile.write('\n')              # ...used default setup -> enter only
                opfile.write('\n')              # Used default selfeter (logN) -> enter only
                opfile.write('thermal.13\n')    # Insert fort file name + enter
                opfile.write('\n')              # Plot the fitting region (default is yes) -> enter only
                opfile.write('\nas')
                for i in range(50):
                    opfile.write('\n\n\n\n')
                opfile.write('\n\n\nn\n\n')
                opfile.close()      
                os.system('vpfit%s < fitcommands > termout'%setup.vpversion)
                os.system('mv vpfit_chunk* chunks/ && rm termout fitcommands')
            # loop over each region and calculate equivalent width
            os.system('ls %s/chunks/ > list'%setup.distpath)
            chunks = np.loadtxt('list',dtype=str)
            os.system('rm list')
            # initialize noise and equivalent width record arrays
            noise,ewidth = [],[]
            # initialise list of velocity spread, number of tied components
            ntied,flag,k = 0,0,0
            # loop over all lines of the input model
            for line in fort13:
                vals = line.split()
                flag = 1 if '*' in line and flag==0 else 2 if '*' in line and flag==1 else flag
                if len(vals)==0: break
                # loop over all fitting region
                if flag==1 and '*' not in line:
                    # check that transition is not external to system
                    if 'external' not in header[k]:
                        # identify ion for given transition
                        trans = header[k].split()[0]
                        # load the data chunk
                        chunk = np.loadtxt(setup.distpath+'chunks/vpfit_chunk%03i.txt'%(k+1),comments='!')
                        # select only chunk of data within fitting region
                        wmin,wmax = float(vals[2]),float(vals[3])
                        imin = abs(chunk[:,0]-wmin).argmin()
                        imax = abs(chunk[:,0]-wmax).argmin()
                        wmin,wmax = chunk[imin,0],chunk[imax,0]
                        # calculate velocity spread of each region
                        spread = 2*(wmax-wmin)/(wmin+wmax)*setup.c
                        # calculate equivalent width
                        chunk = chunk[imin:imax]
                        width = sum([(1-chunk[i,-1])*(chunk[i+1,0]-chunk[i,0])/(setup.zabs+1) for i in range(len(chunk)-1)])
                        # calculate average signal-to-noise
                        chunk = np.delete(chunk,np.where(chunk[:,2]==0)[0],0)
                        snr = np.average(chunk[:,1]/chunk[:,2])
                        # check if transition for this system already stored in array
                        if system+trans not in discard:
                            metals.write('{:<40}'.format(str(system)))
                            metals.write('{:>15}'.format(trans))
                            metals.write('{:>15}'.format(int(spread)))
                            metals.write('{:>15}'.format('%.5f'%width))
                            metals.write('\n')
                            discard.append(system+trans)
                    k += 1
                # loop over each component
                if flag==2 and '*' not in line:
                    # identify ion
                    ion = vals[0]+vals[1] if len(vals[0])==1 else vals[0]
                    # extract absorption redshift
                    zabs = vals[3] if len(vals[0])==1 else vals[2]
                    zabs = float(zabs[:-2]+re.compile(r'[^\d.-]+').sub('',zabs[-2:]))
                    # extract doppler parameter value value
                    bdop = vals[4] if len(vals[0])==1 else vals[3]
                    # increment number of parent tied parameters
                    ntied = ntied + 1 if bdop[-1].islower()==True and abs(zabs-setup.zabs)<0.003 else ntied
                    # # check if ion for this system stored in array
                    # idxs = np.where(np.logical_and(metals[:,0]==system,metals[:,2]==ion))[0]
                    # i = -1 if len(idxs)==0 else idxs[0]
                    # # check if component is tied and not from overlaping system
                    # if i!=-1 and abs(zabs-setup.zabs)<0.003:
                    #     # increment by 1 the number of tied component for this system and ion
                    #     metals[i,-1] = float(metals[i,-1]) + 1
            tied.write('%s  %.5f  %i\n'%(system,setup.zabs,ntied))
        metals.close()
        tied.close()

    def create_dataset(self):
        '''
        Creat dataset from extract data
        '''
        os.chdir(setup.home+'/results/kinematics/')
        # export metallicity results and transition list
        setup.metals = numpy.genfromtxt('list_metals.dat',names=True,dtype=object)
        translist = list(set(setup.metals['trans']))
        # tabulate and store system vs. transition availability
        if '--availability' in sys.argv:
            data = open('list_availability.dat','w')
            data.write('system ')
            for trans in translist:
                data.write('%s '%trans)
            data.write('\n')
            for system in setup.publist['system']:
                data.write(system+' ')
                for trans in translist:
                    idxs = np.where(np.logical_and(setup.metals['system']==system,setup.metals['trans']==trans))[0]
                    flag = 0 if len(idxs)==0 else 1
                    data.write('%i '%flag)
                data.write('\n')
            data.close()
        # check all combination of 5 transition and identify how many system match
        d = numpy.genfromtxt('list_availability.dat',names=True,dtype=float)
        d = pd.DataFrame(d)
        print len(set(itertools.combinations(translist,setup.ntrans))),'combinations...'
        if '--combination' in sys.argv:
            k = 1
            data = open(setup.home+'/results/kinematics/t%i_combination.dat'%setup.ntrans,'w')
            cols = ' '.join(['trans%i '%n for n in range(1,setup.ntrans+1)])
            data.write(cols+' nmatch\n')
            for subset in itertools.combinations(translist,setup.ntrans):
                print '%i/%i'%(k,len(set(itertools.combinations(translist,setup.ntrans))))
                idxs = d[(d[list(subset)] == 1).all(1)].index.tolist()
                data.write(' '.join(list(subset))+' %i\n'%len(idxs))
                k+=1
            data.close()
        # find best combination
        data    = numpy.genfromtxt('t%i_combination.dat'%setup.ntrans,names=True,dtype=object)
        nmax    = max([float(i) for i in data['nmatch']])
        i       = np.where(data['nmatch']=='%i'%nmax)[0][0]
        subset  = [data['trans%i'%n][i] for n in range(1,setup.ntrans+1)]
        idxs    = d[(d[list(subset)] == 1).all(1)].index.tolist()
        data    = numpy.genfromtxt('list_availability.dat',names=True,dtype=object)
        systems = [data['system'][i] for i in idxs]
        # store equivalent width results
        tied = numpy.genfromtxt('list_tied.dat',names=True,dtype=object)
        data = open(setup.home+'/results/kinematics/t%i_data.dat'%setup.ntrans,'w')
        data.write('tied    width    error      zabs    snr    dv dverr\n')
        # loop over every flagged system
        for system in systems:
            # initialise arrays for selected transitions
            spread,ewidth,noise = [],[],[]
            for trans in subset:
                # find row index of transition for given system
                i = np.where(np.logical_and(setup.metals['system']==system,setup.metals['trans']==trans))[0][0]
                # add value of spread, equivalent width and snr
                spread.append(float(setup.metals['spread'][i]))
                ewidth.append(float(setup.metals['ewidth'][i]))
                noise.append(float(setup.metals['snr'][i]))
            # extract absorption redshift and number of tied components
            i = np.where(tied['system']==system)[0][0]
            ntied = float(tied['tied'][i])
            zabs = float(tied['zabs'][i])
            # store relevant information in data array
            data.write( '{:>4}'.format('%.2f'%(ntied)))
            data.write( '{:>9}'.format('%.4f'%np.mean(ewidth)))
            data.write( '{:>9}'.format('%.4f'%np.std(ewidth)))
            data.write('{:>10}'.format('%.4f'%zabs))
            data.write('{:>10}'.format('nan' if math.isnan(np.mean(noise)) else '%.2f'%(np.mean(noise))))
            data.write( '{:>9}'.format('%.2f'%(np.mean(spread))))
            data.write( '{:>9}'.format('%.2f'%(np.std(spread))))
            data.write('\n')
        data.close()
        setup.data = numpy.genfromtxt('t%i_data.dat'%setup.ntrans,names=True,dtype=float)
        
    def equiwidth_vs_dispersion(self):
        '''
        Plot Average Equivalent Width vs. Velocity Dispersion
        '''
        def func(x,a,b):
            return a + b*x
        filename = 't%i_equiwidth_vs_dispersion'%setup.ntrans
        print '\n%s\n'%filename
        self.create_bin2('dv',filename)
        x    = setup.bindata['dv']
        y    = setup.bindata['width']
        col  = setup.bindata['zabs']
        xmin,xmax = 25,190
        ymin,ymax = 0,0.5
        fig = figure(figsize=(7,6))
        plt.subplots_adjust(left=0.1, right=0.87, bottom=0.1, top=0.95, hspace=0, wspace=0)
        ax = subplot(111,xlim=[xmin,xmax],ylim=[ymin,ymax])
        ax.scatter(x,y,marker='o',s=200,edgecolors='none',zorder=1,c=col,
                   cmap=mpl.cm.rainbow,vmin=min(col),vmax=max(col))
        ax.scatter(x,y,marker='o',s=10,edgecolors='none',zorder=3,c='0.5')
        xlabel('Average velocity dispersion (km/s)',fontsize=12)
        ylabel(r'Average equivalent width ($\mathrm{\AA}$)',fontsize=12)
        ax1  = fig.add_axes([0.87,0.1,0.04,0.85])
        cmap = mpl.cm.rainbow
        norm = mpl.colors.Normalize(vmin=min(col),vmax=max(col))
        cb1  = mpl.colorbar.ColorbarBase(ax1,cmap=cmap,norm=norm)
        cb1.set_label(r'$z_\mathrm{abs}$',fontsize=12)
        x    = setup.data['dv']
        xerr = setup.data['dverr']
        y    = setup.data['width']
        yerr = setup.data['error']
        xfit = np.arange(xmin,xmax,0.01)
        coeffs,matcov = curve_fit(func,x,y)
        yfit = func(xfit,coeffs[0],coeffs[1])
        ax.plot(xfit,yfit,color='black',lw=3,ls='dashed',zorder=4)
        print '  Weighted fit   : %.6f +/- %.6f'%(coeffs[1],np.sqrt(matcov[1][1]))
        savefig(filename+'.pdf')
        clf()

    def equiwidth_vs_tied(self):
        '''
        Plot Average Equivalent Width vs. Number of Tied Components
        '''
        def func(x,a,b):
            return a + b*x
        filename = 't%i_equiwidth_vs_tied'%setup.ntrans
        print '\n%s\n'%filename
        self.create_bin2('tied',filename)
        x    = setup.bindata['tied']
        y    = setup.bindata['width']
        col  = setup.bindata['zabs']
        xmin,xmax = 0,12
        ymin,ymax = 0,0.6
        fig = figure(figsize=(7,6))
        plt.subplots_adjust(left=0.1, right=0.87, bottom=0.1, top=0.95, hspace=0, wspace=0)
        ax = subplot(111,xlim=[xmin,xmax],ylim=[0,ymax])
        ax.scatter(x,y,marker='o',s=200,edgecolors='none',zorder=1,c=col,cmap=mpl.cm.rainbow,vmin=min(col),vmax=max(col))
        ax.scatter(x,y,marker='o',s=20,edgecolors='none',zorder=3,c='0.5')
        xlabel('Number of tied components',fontsize=12)
        ylabel(r'Average equivalent width ($\mathrm{\AA}$)',fontsize=12)
        x    = setup.data['tied']
        y    = setup.data['width']
        yerr = setup.data['error']
        xfit = np.arange(xmin,xmax,0.01)
        coeffs,matcov = curve_fit(func,x,y)
        yfit = func(xfit,coeffs[0],coeffs[1])
        ax.plot(xfit,yfit,color='black',lw=3,ls='dashed',zorder=4 )
        print '  Weighted fit   : %.6f +/- %.6f'%(coeffs[1],np.sqrt(matcov[1][1]))
        ax1  = fig.add_axes([0.87,0.1,0.04,0.85])
        cmap = mpl.cm.rainbow
        norm = mpl.colors.Normalize(vmin=min(col),vmax=max(col))
        cb1  = mpl.colorbar.ColorbarBase(ax1,cmap=cmap,norm=norm)
        cb1.set_label(r'$z_\mathrm{abs}$',fontsize=12)
        savefig(filename+'.pdf')
        clf()

    def tiedcomp_vs_redshift(self):
        '''
        Plot Number of tied components vs. Redshift
        '''
        self.create_bin(3)
        def func(x,a,b):
            return a + b*x
        print '\ntiedcomp_vs_redshift.pdf\n'
        x    = setup.bindata[:,3]
        y    = setup.bindata[:,0]
        c    = setup.bindata[:,4]
        xmax = 1.1*max(x)
        ymax = 1.1*max(y)
        fig = figure(figsize=(7,6))
        plt.subplots_adjust(left=0.1, right=0.87, bottom=0.1, top=0.95, hspace=0, wspace=0)
        ax = subplot(111,xlim=[0,xmax],ylim=[0,ymax])
        ax.scatter(x,y,marker='o',s=150,edgecolors='none',zorder=3,\
                   c=c,cmap=mpl.cm.rainbow,vmin=min(c),vmax=max(c))
        xlabel('Absorption redshift',fontsize=12)
        ylabel('Number of tied components',fontsize=12)
        xfit = np.arange(0,xmax,0.01)
        coeffs,matcov = curve_fit(func,x,y)
        yfit = func(xfit,coeffs[0],coeffs[1])
        ax.plot(xfit,yfit,color='black',lw=3,ls='dashed')
        print '  Unweighted fit : %.6f +/- %.6f'%(coeffs[1],np.sqrt(matcov[1][1]))
        ax1  = fig.add_axes([0.87,0.1,0.04,0.85])
        cmap = mpl.cm.rainbow
        norm = mpl.colors.Normalize(vmin=min(c),vmax=max(c))
        cb1  = mpl.colorbar.ColorbarBase(ax1,cmap=cmap,norm=norm)
        cb1.set_label('Signal-to-Noise ratio',fontsize=12)
        savefig('tiedcomp_vs_redshift.pdf')
        clf()

    def equiwidth_vs_redshift(self):
        '''
        Plot Average Equivalent Width vs. Redshift
        ''' 
        self.create_bin(3)
        def func(x,a,b):
            return a + b*x
        print '\nequiwidth_vs_redshift.pdf\n'
        x    = setup.bindata[:,3]
        y    = setup.bindata[:,1]
        yerr = setup.bindata[:,2]
        c    = setup.bindata[:,4]
        xmax = 1.1*max(x)
        ymax = 1.1*max(y)
        fig = figure(figsize=(7,6))
        plt.subplots_adjust(left=0.1, right=0.87, bottom=0.1, top=0.95, hspace=0, wspace=0)
        ax = subplot(111,xlim=[0,xmax],ylim=[0,ymax])
        ax.scatter(x,y,marker='o',s=150,edgecolors='none',zorder=3,c=c,cmap=mpl.cm.rainbow,vmin=min(c),vmax=max(c))
        errorbar(x,y,yerr=yerr,fmt='o',ms=0,c='0.7',zorder=1)
        xlabel('Absorption redshift',fontsize=12)
        ylabel('Average Equivalent Width',fontsize=12)
        xfit = np.arange(0,xmax,0.01)
        coeffs,matcov = curve_fit(func,x,y)
        yfit = func(xfit,coeffs[0],coeffs[1])
        ax.plot(xfit,yfit,color='black',lw=3,ls='dotted')
        print '  Unweighted fit : %.6f +/- %.6f'%(coeffs[1],np.sqrt(matcov[1][1]))
        xfit = np.arange(0,xmax,0.01)
        coeffs,matcov = curve_fit(func,x,y,sigma=yerr)
        yfit = func(xfit,coeffs[0],coeffs[1])
        ax.plot(xfit,yfit,color='black',lw=3,ls='dashed')
        print '  Weighted fit   : %.6f +/- %.6f'%(coeffs[1],np.sqrt(matcov[1][1]))
        ax1  = fig.add_axes([0.87,0.1,0.04,0.85])
        cmap = mpl.cm.rainbow
        norm = mpl.colors.Normalize(vmin=min(c),vmax=max(c))
        cb1  = mpl.colorbar.ColorbarBase(ax1,cmap=cmap,norm=norm)
        cb1.set_label('Signal-to-Noise ratio',fontsize=12)
        savefig('equiwidth_vs_redshift.pdf')
        clf()
    
    def create_bin(self,colname,filename):
        '''
        Bin the data using the defined binning value
        '''
        k = 1
        data = np.array(sorted(setup.data,key=lambda col: col[colname]))
        out  = open(filename+'.dat','w')
        out.write('tied    width    error      zabs    snr    dv dverr\n')
        for i in range(0,len(data),setup.binning):
            ilim  = i+setup.binning if i+setup.binning<=len(data) else len(data)
            tied  = np.average([float(data['tied'][j]) for j in range(i,ilim)])
            error = sum([1/float(data['error'][j])**2 for j in range(i,ilim)])
            width = sum([float(data['width'][j])/float(data['error'][j])**2 for j in range(i,ilim)]) / error
            error = 1 / np.sqrt(error)
            zabs  = np.average([float(data['zabs'][j]) for j in range(i,ilim)])
            snr   = np.average([float(data['snr'][j]) for j in range(i,ilim)])
            dverr = sum([1/float(data['dverr'][j])**2 for j in range(i,ilim)])
            dv    = sum([float(data['dv'][j])/float(data['dverr'][j])**2 for j in range(i,ilim)]) / dverr
            dverr = 1 / np.sqrt(dverr)
            out.write('\n!bin %i\n\n'%k)
            for j in range(i,ilim):
                out.write('!{:>3}'.format('%.2f'%data['tied'][j]))
                out.write( '{:>9}'.format('%.4f'%data['width'][j]))
                out.write( '{:>9}'.format('%.4f'%data['error'][j]))
                out.write('{:>10}'.format('%.4f'%data['zabs'][j]))
                out.write('{:>10}'.format('nan' if math.isnan(data['snr'][j]) else '%.2f'%(data['snr'][j])))
                out.write( '{:>9}'.format('%.2f'%(data['dv'][j])))
                out.write( '{:>9}'.format('%.2f'%(data['dverr'][j])))
                out.write('\n')
            out.write('!--- -------- -------- --------- ------ ----- -----\n')
            out.write( ' {:>4}'.format('%.2f'%tied))
            out.write( ' {:>9}'.format('%.4f'%width))
            out.write( ' {:>9}'.format('%.4f'%error))
            out.write(' {:>10}'.format('%.4f'%zabs))
            out.write(' {:>10}'.format('nan' if math.isnan(snr) else '%.2f'%(snr)))
            out.write( ' {:>9}'.format('%.2f'%(dv)))
            out.write( ' {:>9}'.format('%.2f'%(dverr)))
            out.write('\n')
            k += 1
        
        setup.bindata = numpy.genfromtxt(filename+'.dat',names=True,dtype=float,comments='!')
        
    def create_bin2(self,colname,filename):
        '''
        Bin the data using the defined binning value
        '''
        k = 1
        data = np.array(sorted(setup.data,key=lambda col: col[colname]))
        out  = open(filename+'.dat','w')
        out.write('tied    width      zabs    snr    dv\n')
        for i in range(0,len(data),setup.binning):
            ilim  = i+setup.binning if i+setup.binning<=len(data) else len(data)
            tied  = np.average([float(data['tied'][j]) for j in range(i,ilim)])
            width = np.average([float(data['width'][j]) for j in range(i,ilim)])
            zabs  = np.average([float(data['zabs'][j]) for j in range(i,ilim)])
            snr   = np.average([float(data['snr'][j]) for j in range(i,ilim)])
            dv    = np.average([float(data['dv'][j]) for j in range(i,ilim)])
            out.write('\n!bin %i\n\n'%k)
            for j in range(i,ilim):
                out.write('!{:>3}'.format('%.2f'%data['tied'][j]))
                out.write( '{:>9}'.format('%.4f'%data['width'][j]))
                out.write('{:>10}'.format('%.4f'%data['zabs'][j]))
                out.write('{:>10}'.format('nan' if math.isnan(data['snr'][j]) else '%.2f'%(data['snr'][j])))
                out.write( '{:>9}'.format('%.2f'%(data['dv'][j])))
                out.write('\n')
            out.write('!--- -------- -------- --------- ------ ----- -----\n')
            out.write( ' {:>4}'.format('%.2f'%tied))
            out.write( ' {:>9}'.format('%.4f'%width))
            out.write(' {:>10}'.format('%.4f'%zabs))
            out.write(' {:>10}'.format('nan' if math.isnan(snr) else '%.2f'%(snr)))
            out.write( ' {:>9}'.format('%.2f'%(dv)))
            out.write('\n')
            k += 1
        
        setup.bindata = numpy.genfromtxt(filename+'.dat',names=True,dtype=float,comments='!')
        
    def plot_ion(self):
    
        def func(x,a,b):
            return a + b*x
        ''' ------------------------------------------- '''
        ''' Plot number of tied components vs. redshift '''
        ''' ------------------------------------------- '''
        fig = figure(figsize=(8,12))
        plt.subplots_adjust(left=0.07, right=0.93, bottom=0.05, top=0.93, hspace=0.3, wspace=0.1)
        fig.suptitle('Number of tied components vs. Absorption redshift',fontsize=15)
        for i in range(len(setup.metals)):
            ion   = setup.metals[i,0]
            data  = np.array(sorted(setup.metals[i,2],key=lambda col: col[2]))
            bdata = np.empty((0,2))
            for j in range(0,len(data),setup.binning):
                jlim  = j+setup.binning if j+setup.binning<=len(data) else len(data)
                zabs  = np.average([float(data[k,2]) for k in range(j,jlim)])
                tied  = np.average([float(data[k,0]) for k in range(j,jlim)])
                bdata = np.vstack((bdata,[zabs,tied]))
            ax = subplot(4,2,i+1,xlim=[0,max(bdata[:,0])],ylim=[0,max(bdata[:,1])])
            ax.scatter(bdata[:,0],bdata[:,1],marker='o',s=40,edgecolors='none',zorder=3,c='black',alpha=0.6)
            if len(bdata)>1:
                xfit = np.arange(0,max(bdata[:,0]),0.01)
                coeffs,matcov = curve_fit(func,bdata[:,0],bdata[:,1])
                yfit = func(xfit,coeffs[0],coeffs[1])
                ax.plot(xfit,yfit,color='red',lw=2,ls='dashed')
                setup.linslopeval = coeffs[1]
                setup.linslopeerr = np.sqrt(matcov[1][1])
                #print setup.metals[i,0]+r': %.4f +/- %.4f'%(setup.linslopeval,setup.linslopeerr)
            plt.title(ion,fontsize=12,color='red')
            if (i+1)%2==0:
                ax.yaxis.tick_right()
                ax.yaxis.set_ticks_position('both')
        savefig('tied_vs_redshift_per_ion.pdf')
        clf()
        ''' ----------------------------------------------------------- '''
        ''' Plot average equivalent width vs. number of tied components '''
        ''' ----------------------------------------------------------- '''
        fig = figure(figsize=(8,12))
        plt.subplots_adjust(left=0.07, right=0.93, bottom=0.05, top=0.93, hspace=0.3, wspace=0.1)
        fig.suptitle('Average Equivalent Width vs. Number of tied components',fontsize=15)
        for i in range(len(setup.metals)):
            ion   = setup.metals[i,0]
            data  = np.array(sorted(setup.metals[i,2],key=lambda col: col[0]))
            bdata = np.empty((0,2))
            for j in range(0,len(data),setup.binning):
                jlim  = j+setup.binning if j+setup.binning<=len(data) else len(data)
                tied  = np.average([float(data[k,0]) for k in range(j,jlim)])
                width = np.average([float(data[k,1]) for k in range(j,jlim)])
                bdata = np.vstack((bdata,[tied,width]))
            ax = subplot(4,2,i+1,xlim=[0,max(bdata[:,0])],ylim=[0,max(bdata[:,1])])
            ax.scatter(bdata[:,0],bdata[:,1],marker='o',s=40,edgecolors='none',zorder=3,c='black',alpha=0.6)
            if len(bdata)>1:
                xfit = np.arange(0,max(bdata[:,0]),0.01)
                coeffs,matcov = curve_fit(func,bdata[:,0],bdata[:,1])
                yfit = func(xfit,coeffs[0],coeffs[1])
                ax.plot(xfit,yfit,color='red',lw=2,ls='dashed')
            plt.title(ion,fontsize=12,color='red')
            if (i+1)%2==0:
                ax.yaxis.tick_right()
                ax.yaxis.set_ticks_position('both')
        savefig('equiwidth_vs_tied_per_ion.pdf')
        clf()
        ''' ------------------------------------------ '''
        ''' Plot average equivalent width vs. redshift '''
        ''' ------------------------------------------ '''
        fig = figure(figsize=(8,12))
        plt.subplots_adjust(left=0.07, right=0.93, bottom=0.05, top=0.93, hspace=0.3, wspace=0.1)
        fig.suptitle('Average Equivalent Width vs. Absorption redshift',fontsize=15)
        for i in range(len(setup.metals)):
            ion   = setup.metals[i,0]
            data  = np.array(sorted(setup.metals[i,2],key=lambda col: col[2]))
            bdata = np.empty((0,2))
            for j in range(0,len(data),setup.binning):
                jlim  = j+setup.binning if j+setup.binning<=len(data) else len(data)
                zabs  = np.average([float(data[k,2]) for k in range(j,jlim)])
                width = np.average([float(data[k,1]) for k in range(j,jlim)])
                bdata = np.vstack((bdata,[zabs,width]))
            ax = subplot(4,2,i+1,xlim=[0,max(bdata[:,0])],ylim=[0,max(bdata[:,1])])
            ax.scatter(bdata[:,0],bdata[:,1],marker='o',s=40,edgecolors='none',zorder=3,c='black',alpha=0.6)
            if len(bdata)>1:
                xfit = np.arange(0,max(bdata[:,0]),0.01)
                coeffs,matcov = curve_fit(func,bdata[:,0],bdata[:,1])
                yfit = func(xfit,coeffs[0],coeffs[1])
                ax.plot(xfit,yfit,color='red',lw=2,ls='dashed')
            plt.title(ion,fontsize=12,color='red')
            if (i+1)%2==0:
                ax.yaxis.tick_right()
                ax.yaxis.set_ticks_position('both')
        savefig('equiwidth_vs_redshift_per_ion.pdf')
        clf()
    
