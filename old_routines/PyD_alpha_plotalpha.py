#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def plotalpha():

    os.chdir(self.home+'/results/alpha/')
    
    rc('font', size=10, family='sans-serif')
    rc('axes', labelsize=20, linewidth=0.2)
    rc('legend', fontsize=20, handlelength=10)
    rc('xtick', labelsize=20)
    rc('ytick', labelsize=20)
    rc('lines', lw=0.2, mew=0.2)
    rc('grid', linewidth=0.2)

    #--------------------------------------------------------------------------------------------------

    def alphabin(data,binning=self.binning,visible=False):

        k = 1
        data = np.array(sorted(data,key=lambda col: col[0]))
        bindata = np.empty((0,3),dtype=float)
        for i in range(0,len(data),binning):
            ilim  = i+binning if i+binning<=len(data) else len(data)
            if visible:
                print '\nbin',k,'\n'
            for j in range(i,ilim):
                if visible:
                    print 'z={0:<9}  |  {1:>10} +/- {2:>10}  |  {3:<10}'.format('%.5f'%float(data[j,0]),'%.6f'%float(data[j,1]),'%.6f'%float(data[j,2]),str(data[j,-1]))
            zabs  = np.average([float(data[j,0]) for j in range(i,ilim)])
            alpha = sum([float(data[j,1])/float(data[j,2])**2 for j in range(i,ilim)]) / sum([1/float(data[j,2])**2 for j in range(i,ilim)])
            error = 1 / np.sqrt(sum(1/float(data[j,2])**2 for j in range(i,ilim)))
            if visible:
                print '------------ | ---------------------------'
                print 'z={0:<9}  |  {1:>10} +/- {2:>10}'.format('%.5f'%zabs,'%.6f'%alpha,'%.6f'%error)
            bindata   = np.vstack((bindata,[zabs,alpha,error]))
            k += 1
        return bindata

    #--------------------------------------------------------------------------------------------------
    
    def plotsim(path1,path2,name,clip=False):

        Udata1 = np.loadtxt(self.Crep+path1,usecols=[0,3,4,5],dtype=object)
        Udata2 = np.loadtxt(self.Crep+path2,usecols=[0,3,4,5],dtype=object)
        Bdata1 = alphabin(Udata1)
        Bdata2 = alphabin(Udata2,visible=True)
        
        x1,y1,x2,y2 = Udata2[:,0],Udata2[:,1],Bdata2[:,0],Bdata2[:,1]
        x3,y3,yerr3 = Bdata1[:,0],Bdata1[:,1],Bdata1[:,2]
        x4,y4,yerr4 = Bdata2[:,0],Bdata1[:,1]-Bdata2[:,1],Bdata1[:,2]
            
        fig = figure(figsize=(12,8))
        plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95, hspace=0, wspace=0)
        ax1 = subplot(211,xlim=[0.25,4],ylim=[-2.5,2.5])
        ax1.errorbar(x1,y1,fmt='o',ms=5,lw=3,alpha=0.5,capsize=10,capthick=2,mec='none',color='red',zorder=3)
        ax1.errorbar(x2,y2,fmt='o',ms=20,lw=3,alpha=0.8,capsize=10,capthick=2,mec='none',color='red',zorder=2)
        ax1.errorbar(x3,y3,yerr=yerr3,fmt='o',ms=20,lw=3,alpha=0.7,capsize=10,capthick=2,mec='none',color='black',zorder=1)
        ax1.xaxis.set_minor_locator(MultipleLocator(0.1))
        ax1.yaxis.set_minor_locator(MultipleLocator(0.1))
        ax1.axhline(y=0,color='black')
        ax1.set_title(title)
        plt.setp(ax1.get_xticklabels(), visible=False)
        ylabel(r'$\Delta\alpha/\alpha$ $(10^{-5})$',fontsize=20)
        ax2 = subplot(212,xlim=[0.25,4],ylim=[-2.5,2.5])
        ax2.errorbar(x4,y4,yerr=yerr4,fmt='o',ms=20,lw=3,alpha=0.8,capsize=10,capthick=2,mec='none',color='black')
        ax2.xaxis.set_minor_locator(MultipleLocator(0.1))
        ax2.yaxis.set_minor_locator(MultipleLocator(0.1))
        ax2.axhline(y=0,color='black')
        ylabel(r'$\Delta\alpha/\alpha$ $(10^{-5})$',fontsize=20)
        xlabel('Redshift $z$',fontsize=20)
        savefig(name)
        clf()

    #--------------------------------------------------------------------------------------------------

    def plotfig(path1,path2,name):

        Udata1 = np.loadtxt(self.Crep+path1,usecols=[0,3,4,5],dtype=object)
        Udata2 = np.loadtxt(self.Crep+path2,usecols=[0,3,4,5],dtype=object)
        Bdata1 = alphabin(Udata1)
        Bdata2 = alphabin(Udata2,visible=True)

        x3,y3,yerr3 = Bdata1[:,0],Bdata1[:,1],Bdata1[:,2]
        x4,y4,yerr4 = Bdata2[:,0],Bdata2[:,1],Bdata2[:,2]
            
        fig = figure(figsize=(12,6))
        plt.subplots_adjust(left=0.1, right=0.95, bottom=0.11, top=0.95, hspace=0, wspace=0)
        ax1 = subplot(111,xlim=[0.25,4],ylim=[-2.5,2.5])
        ax1.errorbar(x3,y3,yerr=yerr3,fmt='o',ms=20,lw=3,alpha=0.7,capsize=10,capthick=2,mec='none',color='black')
        ax1.errorbar(x4,y4,yerr=yerr4,fmt='o',ms=20,lw=3,alpha=0.8,capsize=10,capthick=2,mec='none',color='red')
        ax1.xaxis.set_minor_locator(MultipleLocator(0.1))
        ax1.yaxis.set_minor_locator(MultipleLocator(0.1))
        ax1.axhline(y=0,color='black')
        ax1.set_title(title)
        ylabel(r'$\Delta\alpha/\alpha$ $(10^{-5})$',fontsize=20)
        xlabel('Redshift $z$',fontsize=20)
        savefig(name)
        clf()
        
    #--------------------------------------------------------------------------------------------------

    if self.instrument=='uves' or self.plot=='all':
    
        i=1
        title  = 'UVES sample | Real data | Updated results from VPFIT10'
        print 'Plot %i:'%i,title
        if self.plot in ['all',str(i)]:
            sample = 'uves/published/unclipped/v10_chisq1E-06_all/0.000/alphafit_errmod.dat'
            plotfig('uves/reproduce/alphafit_errmod.dat',sample,'uves/da_vs_z_%02i.pdf'%i)
        i+=1
            
        #----------------------------------------------------------------------------------------------
        
        title  = 'UVES sample | Real data | Model at -0.123 m/s/A | DW distortion | MoM results'
        print 'Plot %i:'%i,title
        if self.plot in ['all',str(i)]:
            sample = '/uves/distortion/unclipped/v10_chisq1E-06_all/m0.123/alphafit_errmod.dat'
            plotfig('uves/reproduce/alphafit_errmod.dat',sample,'uves/da_vs_z_%02i.pdf'%i)
        i+=1
        
        #----------------------------------------------------------------------------------------------
        
        title  = 'UVES sample | Real data | Model at -0.123 m/s/A | DW distortion | Best chisq model'
        print 'Plot %i:'%i,title
        if self.plot in ['all',str(i)]:
            sample = '/uves/distortion/bestchisq/v10_chisq1E-06_all/m0.123/alphafit_errmod.dat'
            plotfig('uves/reproduce/alphafit_errmod.dat',sample,'uves/da_vs_z_%02i.pdf'%i)
        i+=1
        
        #----------------------------------------------------------------------------------------------
        
        title  = 'UVES sample | Real data | Model at -0.123 m/s/A | DW distortion | Turbulent only'
        print 'Plot %i:'%i,title
        if self.plot in ['all',str(i)]:
            sample = '/uves/distortion/turbulent/v10_chisq1E-06_all/m0.123/alphafit_errmod.dat'
            plotfig('uves/reproduce/alphafit_errmod.dat',sample,'uves/da_vs_z_%02i.pdf'%i)
        i+=1
        
        #----------------------------------------------------------------------------------------------
        
        title  = 'UVES sample | Real data | Model at -0.123 m/s/A | DW distortion | Thermal only'
        print 'Plot %i:'%i,title
        if self.plot in ['all',str(i)]:
            sample = '/uves/distortion/thermal/v10_chisq1E-06_all/m0.123/alphafit_errmod.dat'
            plotfig('uves/reproduce/alphafit_errmod.dat',sample,'uves/da_vs_z_%02i.pdf'%i)
        i+=1
        
        #----------------------------------------------------------------------------------------------
    
        title  = 'UVES sample | Realistic simulation | Model at +0.123 m/s/A | DW distortion'
        print 'Plot %i:'%i,title
        if self.plot in ['all',str(i)]:
            sample = 'uves/simulation/complex_0.000/v10_chisq1E-06/p0.123/alphafit_errmod.dat'
            plotsim('uves/reproduce/alphafit_errmod.dat',sample,'uves/da_vs_z_%02i.pdf'%i)
        i+=1
    
        #----------------------------------------------------------------------------------------------
    
        title  = 'UVES sample | Real data | Model at -0.117 m/s/A | DW distortion'
        print 'Plot %i:'%i,title
        if self.plot in ['all',str(i)]:
            sample = 'uves/distortion/unclipped/v10_chisq1E-06_all/m0.117/alphafit_errmod.dat'
            plotfig('uves/reproduce/alphafit_errmod.dat',sample,'uves/da_vs_z_%02i.pdf'%i)
        i+=1
            
        #----------------------------------------------------------------------------------------------
        
        title  = 'UVES sample | Simplistic simulation | Model at +0.117 m/s/A | WM distortion'
        print 'Plot %i:'%i,title
        if self.plot in ['all',str(i)]:
            sample = 'uves/simulation/simple_0.000/v10_chisq1E-06_jw/p0.117/alphafit_errmod.dat'
            plotsim('uves/reproduce/alphafit_errmod.dat',sample,'uves/da_vs_z_%02i.pdf'%i)
        i+=1
        
    #==================================================================================================

    if self.instrument=='hires' or self.plot=='all':
    
        i=1
        title  = 'HIRES sample | Real data | Updated results from VPFIT10 | MoM results'
        print 'Plot %i:'%i,title
        if self.plot in ['all',str(i)]:
            sample = 'hires/published/unclipped/v10_chisq1E-06/0.000/alphafit_errmod.dat'
            plotfig('hires/reproduce/all_contrast/alphafit_errmod.dat',sample,'hires/da_vs_z_%02i.pdf'%i)
        i+=1
        
        #----------------------------------------------------------------------------------------------
        
        title  = 'HIRES sample | Real data | Updated results from VPFIT10 | Original model'
        print 'Plot %i:'%i,title
        if self.plot in ['all',str(i)]:
            sample = 'hires/published/original/v10_chisq1E-06/0.000/alphafit_errmod.dat'
            plotfig('hires/reproduce/all_contrast/alphafit_errmod.dat',sample,'hires/da_vs_z_%02i.pdf'%i)
        i+=1
            
        #----------------------------------------------------------------------------------------------
        
        title  = 'HIRES sample | Real data | Corrected updated results'
        print 'Plot %i:'%i,title
        if self.plot in ['all',str(i)]:
            sample = '/temp/alphafit_errmod.dat'
            plotfig('hires/reproduce/all_contrast/alphafit_errmod.dat',sample,'hires/da_vs_z_%02i.pdf'%i)
        i+=1
    
    #==================================================================================================

    if self.instrument=='comb' or self.plot=='all':
    
        i=1
        title  = 'UVES+HIRES sample | Real data | Updated results from VPFIT10 | MoM results'
        print 'Plot %i:'%i,title
        if self.plot in ['all',str(i)]:
            sample = 'comb/all/hires_all-uves_all/alphafit_errmod.dat'
            plotfig('comb/all/hires_all-uves_all/alphafit_errmod.dat',sample,'comb/da_vs_z_%02i.pdf'%i)
        i+=1
        
    #==================================================================================================
    
#    ''' Plot da/a vs. distance to alpha dipole '''
#
#    bindata = alphabin(data_distance)
#    fig = figure(figsize=(10,5))
#    plt.subplots_adjust(left=0.1, right=0.95, bottom=0.15, top=0.95, hspace=0, wspace=0)
#    ax = subplot(111,xlim=[0,180],ylim=[-2,2])
#    ax.errorbar(bindata[:,1],bindata[:,2],yerr=bindata[:,3],fmt='o',ms=10,lw=3,c='black',capsize=10)
#    ax.errorbar(bindata[:,1],bindata[:,4],yerr=bindata[:,5],fmt='o',ms=10,lw=3,alpha=0.8,c='red',capsize=10)
#    ax.xaxis.set_minor_locator(MultipleLocator(1))
#    ax.yaxis.set_minor_locator(MultipleLocator(0.1))
#    ax.axhline(y=0,color='black')
#    xlabel(r'$\Theta$, angle from dipole ('+str(xdip)+','+str(ydip)+')',fontsize=15)
#    ylabel(r'MoM $\Delta\alpha/\alpha$ $(10^{-5})$',fontsize=15)
#    savefig('da_vs_angle-'+self.test+'-'+self.distortion+'-'+str(binning)+'.pdf')
#    clf()    
#
#    ''' Plot whole sky map '''
#    
#    fig = figure(figsize=(11.69,8.27))
#    plt.subplots_adjust(left=0.05, right=0.95, bottom=0.1, top=0.95, hspace=0, wspace=0)
#    
#    m = Basemap(projection='moll',lat_0=0,lon_0=180)
#    m.drawparallels(np.arange(-90.,90,10.))
#    m.drawmeridians(np.arange(0,360,20))
#    sbig,smax = 0,5
#    for i in range (len(self.restable)):
#        if self.restable['MoM_alpha'][i] not in ['0.0000E+00','-']:
#            sbig = sbig+1 if float(self.restable['MoM_alpha'][i])>smax else sbig
#            size = 50 if abs(float(self.restable['MoM_da'][i]))>5 else float(self.restable['MoM_da'][i])/5*50
#            xpt,ypt = m(float(self.restable['ra'][i]),float(self.restable['dec'][i]))
#            mark  = 's' if self.restable['sample'][i] in keck else 'o'
#            scatter(xpt,ypt,c=float(self.restable['MoM_alpha'][i]),edgecolor='none',\
#                    s=size,marker=mark,cmap=mpl.cm.cool,vmin=-smax,vmax=smax)
#        
#    ax1  = fig.add_axes([0.3,0.1,0.4,0.05])
#    cmap = mpl.cm.cool
#    norm = mpl.colors.Normalize(vmin=-smax,vmax=smax)
#    cb1  = mpl.colorbar.ColorbarBase(ax1,cmap=cmap,norm=norm,orientation='horizontal')
#    cb1.set_label(r'$\Delta\alpha/\alpha$ $(10^{-5})$')
#    
#    print sbig,'measurements larger than absolute 5e-5'
#    savefig('map.pdf')
#    clf()

#==================================================================================================================
