#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def plottalk():
    
    self.path = self.home+'/results/figures/talk/'
    os.chdir(self.path)
    
    slope     = 0.200
    xmin,xmax = 2800,11000
    ymin,ymax = -400,400
    xlist     = [3000,4000,5000,6000,7000,8000,9000,10000]
    ylist     = [-300,-200,-100,0,100,200,300]
    system    = 'J043037-485523/1.35560/UVES_squader'
    quasar    = 'J043037-485523'
    sample    = 'UVES_squader'
            
    print system
    ions  = np.loadtxt(self.fitdir+system+'/model00/model/turbulent.13',delimiter='\n',dtype=str)
    trans = []
    flag  = 0
    for line in ions:
        if '*' in line and flag==1:
            break
        elif '*' in line:
            flag = 1
        else:
            trans.append((float(line.split()[2])+float(line.split()[3]))/2)
            
    for nexp in range(1,19):
        
        fig = figure(figsize=(7,4))
        plt.subplots_adjust(left=0.12, right=0.95, bottom=0.12, top=0.95, hspace=0, wspace=0.1)
        ax = plt.subplot(211,xlim=[xmin,xmax],ylim=[ymin,ymax])
        
        ''' Distortion models and fitting regions '''    
        
        exp1 = np.empty((0,4))
        exp2 = np.empty((0,4))
        
        n = 0
        pos = np.where(self.uvesexp['name']==quasar)[0][0]
        for l in range (pos,len(self.uvesexp)):
            if self.uvesexp['name'][l]!=quasar or n==nexp:
                break
            for k in range(len(self.uvesset)):
                cond1 = self.uvesset['cent'][k]==self.uvesexp['cent'][l]
                cond2 = self.uvesset['arm'][k]==self.uvesexp['arm'][l]
                cond3 = self.uvesset['mode'][k]==self.uvesexp['mode'][l]
                if cond1==cond2==cond3==True:
                    wbeg = 10*float(self.uvesset['TS_min'][k])
                    cent = 10*float(self.uvesset['cent'][k])
                    wend = 10*float(self.uvesset['TS_max'][k])
                    time = float(self.uvesexp['exptime'][l])
                    if cent not in [float(j) for j in exp1[:,2]]:
                        color = 'blue' if self.uvesexp['arm'][l]=='BLUE' else 'red'
                        exp1 = np.vstack((exp1,[color,wbeg,cent,wend]))
                    exp2 = np.vstack((exp2,[time,wbeg,cent,wend]))
                    break
            n+=1
            
        list2 = np.array([float(exp) for exp in exp2[:,2]])
        for k in range (len(exp1)):
            color   = exp1[k,0]
            wastart = float(exp1[k,1])
            wacent  = float(exp1[k,2])
            waend   = float(exp1[k,3])
            x       = np.arange(wastart,waend,1)
            y       = slope*(x-wacent)
            ax.plot(x,y,color=color,lw=1.5,zorder=3)
            ax.axvline(x=wastart,color='black',zorder=2,lw=1,ls='dotted')
            ax.axvspan(wastart,waend,facecolor='yellow',alpha=0.1,zorder=1)
            ax.axvline(x=waend,color='black',zorder=2,lw=1,ls='dotted')
            ax.scatter(wacent,0,color='black',marker='d',edgecolors='none',s=25,zorder=4)
            counts = len(np.where(list2==float(exp1[k,2]))[0])
            ax.text(wacent,50,'%i'%counts,color='black',fontsize=12,ha='center')
            
        ax.axhline(y=0,ls='dotted',color='black')
        ax.xaxis.set_major_locator(plt.FixedLocator(xlist))
        ax.yaxis.set_major_locator(plt.FixedLocator(ylist))
        plt.setp(ax.get_xticklabels(), visible=False)
        ylabel('Velocity shift (m/s)',size=10)
        
        #for i in range(len(trans)):
        #    ax.axvline(x=trans[i],ymin=0.85,ymax=0.9,color='red',lw=1)
        
        ''' Correction function '''
        
        ax = plt.subplot(212,xlim=[xmin,xmax],ylim=[ymin,ymax])
        
        for k in range (len(exp1)):
            wastart = float(exp1[k,1])
            wacent  = float(exp1[k,2])
            waend   = float(exp1[k,3])
            ax.axvline(x=wastart,color='black',zorder=2,lw=1,ls='dotted')
            ax.axvspan(wastart,waend,facecolor='yellow',alpha=0.1)
            ax.axvline(x=waend,color='black',zorder=2,lw=1,ls='dotted')
            
        x,y = np.arange(3000,10500,1),[]
        for wa in x:
            vdist,texp = [],[]
            for i in range (len(exp2)):
                if exp2[i,1] < wa < exp2[i,3]:
                    vdist.append(slope*(wa-exp2[i,2]))
                    texp.append(np.sqrt(exp2[i,0]))
            if vdist!=[]:
                y.append(sum(np.array(texp)*np.array(vdist))/sum(np.array(texp)))
            else:
                y.append(None)
        ax.plot(x,y,color='black',lw=1.5,zorder=1)
        ax.axhline(y=0,ls='dotted',color='black')
        ax.xaxis.set_major_locator(plt.FixedLocator(xlist))
        ax.yaxis.set_major_locator(plt.FixedLocator(ylist))
    
        #for i in range(len(trans)):
        #    ax.axvline(x=trans[i],ymin=0.85,ymax=0.9,color='red',lw=1)
    
        xlabel('Wavelength ($\AA$)',size=10)
        ylabel('Velocity shift (m/s)',size=10)
    
        savefig('%02i.pdf'%nexp)
        plt.close(fig)

#==================================================================================================================
