#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def plotmodels():

    os.chdir(self.home+'/results/models/')
    
    print '|- Plot Whitmore distortion function...',

    slope = 0.117
    xmin,xmax = 3000,10000

    fig = figure(figsize=(7,4))
    plt.subplots_adjust(left=0.13, right=0.95, bottom=0.13, top=0.95, hspace=0, wspace=0.1)
    ax = plt.subplot(111,xlim=[xmin,xmax],ylim=[-250,250])

    ax.axhline(y=0,ls='dotted',color='black')
    
    x = np.arange(xmin,3259.1,1)
    y = [-slope*3900+slope*3259.1 for i in range(len(x))]
    ax.plot(x,y,'black',ls='dashed',lw=2)

    x0 = 3900
    x = np.arange(3259.1,4518.8,1)
    y = slope*(x-x0)
    ax.plot(x,y,'blue',lw=2)

    x  = np.arange(4518.8,4726.9,1)
    y1 = -slope*3900 + slope*4518.8
    y2 = -slope*5800 + slope*4726.9
    a  = (y2-y1)/(4726.9-4518.8)
    b  = 4518.8 - y1/a
    y  = a*(x-b)
    ax.plot(x,y,'black',ls='dashed',lw=2)

    x0 = 5800
    x = np.arange(4726.9,6834.9,1)
    y = slope*(x-x0)
    ax.plot(x,y,'red',lw=2)

    x = np.arange(6834.9,xmax,1)
    y = [-slope*5800+slope*6834.9 for i in range(len(x))]
    ax.plot(x,y,'black',ls='dashed',lw=2)

    ylabel('Velocity shift (m/s)',fontsize=10)
    xlabel('Wavelength ($\AA$)',fontsize=10)
    
    savefig('uves_whitmore.pdf')
    close()

    print 'done!'

    print '|- Plot UVES distortions models on top of Whitmore curves...',
    
    fig = figure(figsize=(11.69,8.27))
    plt.subplots_adjust(left=0.148, right=0.936, bottom=0.25, top=0.807, hspace=0, wspace=0)
    ax1 = plt.subplot(111,xlim=[3000,7000],ylim=[-1000,1000])
    ax1.get_xaxis().set_visible(False)
    ax1.get_yaxis().set_visible(False)
    ax1.axhline(y=0)
    superimpose(self.pathdata+'uves.png',0.15)

    for i in np.arange(-0.5,0.5,0.1):
        
        x0 = self.uvesblue
        x = np.arange(3000,5000,1)
        slope = i
        y = -slope*x0 + slope*x
        ax1.plot(x,y,'grey',lw=2,zorder=2,alpha=0.7)
        x0 = self.uvesred
        x = np.arange(4200,11000,1)
        slope = i
        y = -slope*x0 + slope*x
        ax1.plot(x,y,'grey',lw=2,zorder=2,alpha=0.7)
    
    savefig('uves.pdf')
    close()

    print 'done!'
    
    print '|- Plot HIRES slope distortion models on top of Whitmore curves...',
    
    fig = figure(figsize=(11.69,8.27))
    plt.subplots_adjust(left=0.133, right=0.919, bottom=0.235, top=0.791, hspace=0, wspace=0)
    ax1 = plt.subplot(111,xlim=[3300,8000],ylim=[-730,870])
    ax1.get_xaxis().set_visible(False)
    ax1.get_yaxis().set_visible(False)
    ax1.axhline(y=0)
    superimpose(self.pathdata+'hires.png',0.19)
    
    for i in np.arange(-0.5,0.5,0.1):
        
        x0 = 5500
        x = np.arange(3300,8000,1)
        slope = i
        y = -slope*x0 + slope*x
        ax1.plot(x,y,'grey',lw=2,zorder=2,alpha=0.7)            
    
    savefig('hires_slope.pdf')
    close()

    print 'done!'

    print '|- Plot HIRES amplitude distortion models on top of Whitmore curves...',
    
    fig = figure(figsize=(11.69,8.27))
    plt.subplots_adjust(left=0.133, right=0.919, bottom=0.235, top=0.791, hspace=0, wspace=0)
    ax1 = plt.subplot(111,xlim=[3300,8000],ylim=[-730,870])
    ax1.get_xaxis().set_visible(False)
    ax1.get_yaxis().set_visible(False)
    ax1.axhline(y=0)
    superimpose(self.pathdata+'hires.png',0.19)
    
    for i in self.hires_amp:
        
        amplitude = i
        x0,x1 = 3800,5300
        x  = np.arange(3000,x0,1)
        y = np.array([amplitude]*(x0-3000))
        ax1.plot(x,y,'grey',lw=2,zorder=2,alpha=0.7)
        x = np.arange(x0+13,x1-13,1)
        y = amplitude * (np.cos((x-x0)*np.pi/(x1-x0)))**2
        ax1.plot(x,y,'grey',lw=2,zorder=2,alpha=0.7)
        x  = np.arange(x1,8000,1)
        y = np.array([amplitude]*(8000-x1))
        ax1.plot(x,y,'grey',lw=2,zorder=2,alpha=0.7)
    
    savefig('hires_amplitude.pdf')
    close()

    print 'done!'

    print '|- Plot all UVES distortion models for all wavelength settings...',

    fig = figure(figsize=(12,7))
    plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95, hspace=0, wspace=0.1)
    ax = plt.subplot(111,xlim=[3000,10500],ylim=[-1000,1000])
    
    cent = np.empty((0,2))
    for i in range (len(self.uvesset)):
        if float(self.uvesset['cent'][i]) not in cent[:,0]:
            cent = np.vstack((cent,[float(self.uvesset['cent'][i])*10,0]))
            
    for k in range (len(self.uvesset)):
        for i in np.arange(-0.5,0.5,0.1):
            x0 = float(self.uvesset['cent'][k])*10
            x = np.arange(float(self.uvesset['TS_min'][k])*10,float(self.uvesset['TS_max'][k])*10,1)
            slope = i
            y = -slope*x0 + slope*x
            ax.plot(x,y,'blue',lw=1,alpha=0.06,\
                    c=cm.rainbow((float(self.uvesset['cent'][k])-346)/(860-346)),\
                    zorder=1)
                    
    ax.scatter(cent[:,0],cent[:,1],color='black',marker='d',edgecolors='none',s=15,zorder=2)
    ax.axhline(y=0,ls='dotted',color='black')
    xlabel('VLT-UVES detector wavelength range ($\AA$)',size=10)
    ylabel('Velocity shift (m/s)',size=10)
    
    savefig('uves_curves.pdf')
    clf()
    
    print 'done!'
    
    print '|- Plot HIRES slope distortion models only...',
    
    fig = figure(figsize=(7,4))
    plt.subplots_adjust(left=0.13, right=0.95, bottom=0.13, top=0.95, hspace=0, wspace=0.1)
    ax = plt.subplot(1,1,1,xlim=[3300,8000],ylim=[-250,250])
    x0 = 5500
    x = np.arange(3300,8000,1)
    slope = 0.1
    y = -slope*x0 + slope*x
    ax.plot(x,y,'blue',lw=2)
    ax.axhline(y=0,ls='dotted',color='black')
    ylabel('Velocity shift (m/s)',fontsize=10)
    xlabel('Wavelength ($\AA$)',fontsize=10)
    
    savefig('hires_curves.pdf')
    close()
    
    print 'done!'
    
    print '|- Plot distortion overlap...',

    s   = 0.2
    fig = figure(figsize=(7,4))
    plt.subplots_adjust(left=0.13, right=0.95, bottom=0.13, top=0.95, hspace=0, wspace=0)
    
    ax  = plt.subplot(2,2,1,xlim=[2500,7500],ylim=[-250,250])
    x1  = np.arange(3259.1,4518.8,0.1)
    y1  = -s*3900+s*x1
    x2  = np.arange(4726.9,6834.9,0.1)
    y2  = -s*5800+s*x2
    ax.plot(x1,y1,'blue',lw=2)
    ax.plot(x2,y2,'red',lw=2)
    ax.axhline(y=0,ls='dotted',color='black')
    ax.axvline(x=3259.1,ls='dotted',color='black')
    ax.axvline(x=4518.8,ls='dotted',color='black')
    ax.axvline(x=4726.9,ls='dotted',color='black')
    ax.axvline(x=6834.9,ls='dotted',color='black')
    ylabel('Velocity shift (m/s)',fontsize=10)
    plt.setp(ax.get_xticklabels(), visible=False)
    
    ax  = plt.subplot(2,2,3,xlim=[2500,7500],ylim=[-250,250])
    x1  = np.arange(3259.1,4518.8,0.1)
    y1  = -s*3900+s*x1
    x2  = np.arange(4726.9,6834.9,0.1)
    y2  = -s*5800+s*x2
    ax.plot(x1,y1,'black',lw=2)
    ax.plot(x2,y2,'black',lw=2)
    ax.axhline(y=0,ls='dotted',color='black')
    ax.axvline(x=3259.1,ls='dotted',color='black')
    ax.axvline(x=4518.8,ls='dotted',color='black')
    ax.axvline(x=4726.9,ls='dotted',color='black')
    ax.axvline(x=6834.9,ls='dotted',color='black')
    ylabel('Velocity shift (m/s)',fontsize=10)
    xlabel('Wavelength ($\AA$)',fontsize=10)
    
    ax  = plt.subplot(2,2,2,xlim=[2500,7500],ylim=[-250,250])
    x1  = np.arange(3732.4,4999.4,0.1)
    y1  = -s*4370+s*x1
    x2  = np.arange(4583.5,6686.3,0.1)
    y2  = -s*5640+s*x2
    ax.plot(x1,y1,'blue',lw=2)
    ax.plot(x2,y2,'red',lw=2)
    ax.axhline(y=0,ls='dotted',color='black')
    ax.axvline(x=3732.4,ls='dotted',color='black')
    ax.axvline(x=4999.4,ls='dotted',color='black')
    ax.axvline(x=4583.5,ls='dotted',color='black')
    ax.axvline(x=6686.3,ls='dotted',color='black')
    plt.setp(ax.get_xticklabels(), visible=False)
    plt.setp(ax.get_yticklabels(), visible=False)
    
    ax  = plt.subplot(2,2,4,xlim=[2500,7500],ylim=[-250,250])
    x   = np.arange(3732.4,6686.3,0.01)
    y   = []
    for i in x:
        if i<=4583.5:
            y.append(-s*4370+s*i)
        if 4583.5<i<4999.4:
            y.append((-s*4370+s*i-s*5640+s*i)/2)
        if 4999.4<=i:
            y.append(-s*5640+s*i)
    ax.plot(x,y,'black',lw=2)
    ax.axhline(y=0,ls='dotted',color='black')
    ax.axvline(x=3732.4,ls='dotted',color='black')
    ax.axvline(x=4999.4,ls='dotted',color='black')
    ax.axvline(x=4583.5,ls='dotted',color='black')
    ax.axvline(x=6686.3,ls='dotted',color='black')
    xlabel('Wavelength ($\AA$)',fontsize=10)
    plt.setp(ax.get_yticklabels(), visible=False)

    savefig('overlap.pdf')
    close()
    
    print 'done!'
    
#==================================================================================================================

def superimpose(picture,zoom):
        
    ax2 = axes([0,0,1,1])
    arr_lena = read_png(picture)
    imagebox = OffsetImage(arr_lena, zoom=zoom)
    ab = AnnotationBbox(imagebox,(.5,.5),xycoords='data',boxcoords='offset points',frameon=False)
    ax2.add_artist(ab)
    ax2.get_xaxis().set_visible(False)
    ax2.get_yaxis().set_visible(False)
    ax2.patch.set_alpha(0)

#==================================================================================================================
