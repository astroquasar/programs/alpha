#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#===============================================================================================

def plotpaper():

    self.path = self.home+'/results/figures/paper/'
    os.chdir(self.path)
    
    # Copy relevant file from systems folder
    
    syspath = 'J043037-485523/1.35560/UVES_squader'
    simpath = 'model01/sims/complex_p0.200_snr1E-03_null'
    dispath = 'v10_chisq1E-05_step5E-03_prev/p0.200'
    os.system('cp -r '+self.fitdir+syspath+'/'+simpath+'/model/data/ data/')
    os.system('cp '+self.fitdir+syspath+'/'+simpath+'/'+dispath+'/turbulent/header.dat .')
    os.system('cp '+self.fitdir+syspath+'/'+simpath+'/'+dispath+'/turbulent/atom.dat .')
    os.system('cp '+self.fitdir+syspath+'/'+simpath+'/'+dispath+'/turbulent/vp_setup.dat .')
    os.system('cp '+self.fitdir+syspath+'/'+simpath+'/'+dispath+'/turbulent/turbulent_ini.13 fort.13')
    fort13 = np.loadtxt('fort.13',dtype=str,delimiter='\n')

    # Create chunks

    trans = np.loadtxt('metals.dat',dtype=str)    
    opfile = open('fitcommands','w')
    opfile.write('d\n\n\n\n\n')
    for i in range(len(trans)):
        if i==0:
            opfile.write('\nas\n\n\n\n')
        elif i==len(trans)-1:
            opfile.write('\n\n\nn\n\n')
        else:
            opfile.write('\n\n\n\n')
    opfile.close()    
    os.environ['ATOMDIR']='./atom.dat'
    os.environ['VPFSETUP']='./vp_setup.dat'
    os.system('vpfit10 < fitcommands > termout')
    os.system('rm fitcommands termout')
    os.system('mkdir -p chunks/')
    os.system('mv vpfit_chunk* chunks/')

    # Create the plot
    
    fig = figure(figsize=(10,6))
    subplots_adjust(left=0.05,right=0.97,bottom=0.1,top=0.95,wspace=0,hspace=0)
    
    rc('font',   size=10, family='serif')
    rc('axes',   labelsize=10, linewidth=0.2)
    rc('legend', fontsize=10, handlelength=5)
    rc('xtick',  labelsize=10)
    rc('ytick',  labelsize=10)
    rc('lines',  lw=0.2, mew=0.2)
    rc('grid',   linewidth=0.2)
    
    #pos = [ 1, 7,13,10,16, 5,11,17,12,6,18,2,8,14,3,9,15,4]
    pos  = [ 1, 4, 7, 6, 9,12,]
    
    for i in range(18):
        
        print 'Processing',trans[i,0],'%.2f'%float(trans[i,1])
        
        wamid  = float(trans[i,1])*(1+1.3554)
        spec   = np.loadtxt(self.home+'/results/figures/chunks/vpfit_chunk%03i.txt'%(i+1),comments='!')
        ipix   = abs(spec[:,0]-wamid).argmin()
        pix1   = spec[ipix,0]
        pix2   = (spec[ipix,0]+spec[ipix-1,0])/2
        dempix = 2*(pix1-pix2)/(pix1+pix2)*self.c
        #ax     = fig.add_subplot(3,6,pos[i])
        ax     = fig.add_subplot(6,3,pos[i])
        vel    = 2*(spec[:,0]-wamid)/(spec[:,0]+wamid)*self.c
        ax.plot(vel+dempix,spec[:,1],color='black',lw=0.2,drawstyle='steps')
        ax.plot(vel,spec[:,1],color='black',lw=0.2)
        ax.plot(vel,spec[:,3],color='red',lw=0.2)
        ax.axhline(y=0,color='black',ls='dashed',lw=0.2)
        ax.axhline(y=1,color='black',ls='dashed',lw=0.2)
        ax.xaxis.set_major_locator(plt.FixedLocator([-50,0,50]))
        ax.yaxis.set_major_locator(plt.FixedLocator([0,1]))
        if pos[i] not in [1,7,13]:
            plt.setp(ax.get_yticklabels(), visible=False)
        if pos[i] not in [13,14,15,16,17,18]:
            plt.setp(ax.get_xticklabels(), visible=False)
        ax.axhline(y=1.4,color='black',zorder=2)
        ax.axhline(y=1.5,color='black',ls='dotted',zorder=2)
        ax.axhline(y=1.6,color='black',zorder=2)
        res = (spec[:,1]-spec[:,3])/spec[:,2]/10+1.5
        ax.plot(vel+dempix,res,drawstyle='steps',color='black',zorder=3)
        ax.text(0,-0.4,trans[i,0]+' %.2f'%float(trans[i,1]),fontsize=10,color='black',ha='center')
        
        # Prepare high dispersion wavelength array
        
        self.dv = 200
        self.dispersion = 0.01
        start  = wamid * (2*self.c-self.dv) / (2*self.c+self.dv)
        end    = wamid * (2*self.c+self.dv) / (2*self.c-self.dv)
        val    = 1
        wave   = [start-2]
        dv     = self.dispersion
        while wave[-1]<end+2:
            wave.append(wave[-1]*(2*self.c+dv)/(2*self.c-dv))
        wave   = np.array(wave)
        vel    = 2*(wave-wamid)/(wave+wamid)*self.c
        model  = [1]*len(vel)
        
        for line in fort13[len(trans)+2:]:
            
            linesplit = line.split()
            complist = np.empty((0,2))
            N = float(re.compile(r'[^\d.-]+').sub('',linesplit[1]))
            z = float(re.compile(r'[^\d.-]+').sub('',linesplit[2]))
            b = float(re.compile(r'[^\d.-]+').sub('',linesplit[3]))
            
            for p in range(len(self.atom)):

                cond1 = linesplit[0]==self.atom[p,0]
                cond2 = linesplit[0] not in ['<>','>>','__','<<']
                cond3 = spec[0,0] < (1+z)*float(self.atom[p,1]) < spec[-1,0]

                if cond1 and cond2 and cond3:

                    lambda0 = float(self.atom[p,1])
                    f       = float(self.atom[p,2])
                    gamma   = float(self.atom[p,3])
                    profile = self.voigt(N,b,wave/(z+1),lambda0,gamma,f)
                    model   = model*profile
                    vsig    = 2.54796545/dv
                    conv    = gaussian_filter1d(profile,vsig)
                    plot(vel,conv,lw=0.1,color='orange')

        vsig  = 2.54796545/dv
        model = gaussian_filter1d(model,vsig)
        plot(vel,model,lw=1.,color='orange',alpha=.5,zorder=1)
        
        for line in fort13:
            if line.split()[0]==trans[i,0]:                
                tie  = line.split()[2][-1]
                zabs = float(re.compile(r'[^\d.-]+').sub('',line.split()[2]))
                wabs = float(trans[i,1])*(1+zabs)
                vabs = 2*(wabs-wamid)/(wabs+wamid)*self.c
                ax.plot([vabs,vabs],[1.1,1.2],color='red',lw=0.5)
                
        ax.set_xlim([-100,100])
        ax.set_ylim([-0.7,1.9])

        if pos[i] in [1,7,13]:
            ax.set_ylabel('Normalized Flux',fontsize=8)
        if pos[i] in [13,14,15,16,17,18]:
            ax.set_xlabel('Velocity relative\nto z=1.3554 (km/s)',fontsize=8)
        
    savefig('plot.pdf')
    
#===============================================================================================
