#!/usr/bin/env python

import PyD_alpha_init as self
from PyD_alpha_init import *

#==================================================================================================================

def check_uves_head():

    a = np.empty((0,2))
    for i in range(len(self.uvesexp)):

        header = np.loadtxt(self.uveshead+self.uvesexp['dataset'][i]+'.dat',\
                            dtype='str',delimiter='\n',comments=None)

        arm = mode = grating = cent = 0
        
        for l in range(len(header)):
            if 'Optical path used' in header[l]:
                arm = header[l].split('/')[0].split('=')[1].strip().replace("'","")
            if 'Instrument mode used' in header[l]:
                mode = header[l].split('/')[0].split('=')[1].strip().replace("'","")
            if 'Grating common name' in header[l]:
                grating = header[l].split('/')[0].split('=')[1].strip().replace("'","")
            if 'Grating central wavelength' in header[l]:
                cent = str(int(float(header[l].split('/')[0].split('=')[1].strip().replace("'",""))))

        if arm==0 or mode==0 or grating==0 or cent==0:
            print 'Problem with',self.uvesexp['dataset'][i]
            quit()
            
        if arm+'_'+cent+'_'+mode+'_'+grating not in a[:,0]:
            a = np.vstack((a,[arm+'_'+cent+'_'+mode+'_'+grating,1]))
#            print '{:<10}'.format(arm),'{:<10}'.format(cent),'{:<10}'.format(mode),'{:<10}'.format(grating)
        else:
            k = np.where(a[:,0]==arm+'_'+cent+'_'+mode+'_'+grating)[0][0]
            a[k,1] = str(int(a[k,1])+1)

#        if mode=='DICHR#1' and grating=='CD#4':
#            print ''
#            print 'culprit:','{:<20}'.format(self.uvesexp['dataset'][i])
#            print ''

    a  = np.array(sorted(a,key=operator.itemgetter(0)))
    for i in range(len(a)):
        print a[i,0],' - ',a[i,1]

#==================================================================================================================
