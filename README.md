# Analysis tools for da/a measurements

[![License MIT](https://img.shields.io/badge/License-MIT-blue.svg)](http://opensource.org/licenses/MIT)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.438200.svg)](https://doi.org/10.5281/zenodo.438200)

## Installation

All required packages can be installed as follows:

```bash
sudo pip-2.7 install -r requirements.txt
```

## Acknowledgment

Please acknowledge the use of this software in resulting publications by using the following citation:

```text
@misc{vincent_dumont_2017_438200,
  author       = {Vincent Dumont},
  title        = {Alpha and distortion analysis tools},
  month        = mar,
  year         = 2017,
  doi          = {10.5281/zenodo.438200},
  url          = {https://doi.org/10.5281/zenodo.438200}
}
 Edit
```

## License

(The MIT License)

Copyright (c) 2013-2017 Vincent Dumont

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
